# Julia Package JASM

## Quickstart
You can add the unregisted package by typing in the Julia package editor:
    add https://gitlab.com/Viraja/JASM.jl

With this package you can generate the data. The
python scripts to display the data can be cloned
with git.

## Authors

* **Juan Pablo Carbajal**
* **Mariane Schneider**

## License

This project is licensed under the GPLv3+ license - see the [LICENSE](LICENSE) file for details

## Repository structure
The folder structure of the repository is:

    .
    ├── data
    ├── doc
    ├── julia
    └── python

The `data` folder is meant to store raw data and the output of scripts.
It is actually a placeholder, no data should be added to the repository.
The data was used for the corresponding publication can be found here:
 [![DOI](https://zenodo.org/badge/DOI/10.25678/0000dd.svg)](https://doi.org/10.25678/0000dd)


The `doc` folder contains documents used during the development of this package.

The `python` folder contains scripts implementing the analysis of the data using the features from the [sbrfeatures](https://gitlab.com/sbrml/sbrfeatures) module.

The `julia` folder contains development code in Julia v1.7

## Julia
The current code is meant to run in an isolated environment
To set the environment run

    julia install_deps.jl

This will set up a Project folder. Do this only once.
After you can activate the local environment by starting julia as follows.
This will install you all the required packages and set up an environment
that is identical to the one we are using.

    julia --project

Once the project is started you can run a script with:

    include("s_myscript.jl")

## Pluto
To run the .jl files which contain the word pluto import Pluto in the
julia project.

    import Pluto

And then run it.

    Pluto.run()

## Python
To run python the scrips in Python. First install the sbr feature package:

    pip install https://gitlab.com/sbrml/sbrfeatures

Then go for example to ipython and use

    run s_skriptname.py ../julia

you have to add

    ../julia

because it sets the path for the data to be read correctly if you use the folder structure as described above. In case of an error, please check if the
data really is in the data folder or not.

## Pronunciation of JASM
JASM comes from Julia ASM (activated sludge model). It is pronounced as Shazam.

## Contact
Please contact myschneider@meiru.ch if you have any questions.
