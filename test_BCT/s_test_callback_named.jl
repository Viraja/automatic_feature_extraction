# Test callback function for implementing into an SBR
using BioChemicalTreatment
using DifferentialEquations
using ModelingToolkit
using Plots
using Symbolics
@variables t
@variables SS(t) XS(t) HA(t) HB(t)
const V = 1
df_equations = Dict(
    "SS" => -1*SS,
    "XS" => -1*XS + V*sin(2*pi*t),
    "HB" => HA,
    "HA" => -1*HB)


function df_constructor(equations)
   du = []
   for (idx, (var, rate)) in enumerate(equations)
      push!(eqs, du[idx] ~ rate)
   end
   return du 
end

init = Dict([
    "SS" => 9.0,
    "HA" => 1.0,
    "HB" => 3.0,
    "XS" => 4.5
    ])

u0 = [9.0, 4.5, 1.0, 3.0]
const V = 1
prob = ODEProblem(f, u0, V, (0.0, 10.0))

"""
test
"""
function discrete_inflow(prob, trigger_time, state_delta)
   u = [:SS, :XS, :HA, :HB]
   condition(u, t, delta_state) = t == trigger_time
   affect!(integrator) = begin
      for i in 1:length(state_delta)
         integrator.u[i] += state_delta[i]
      end
   end
   cb = DiscreteCallback(condition, affect!)
end

function discrete_outflow(prob, trigger_time, state_delta)
   u = [:HA, :XS, :HB, :SS]
   condition(u, t, delta_state) = t == trigger_time
   affect!(integrator) = begin
      for i in 1:length(state_delta)
         integrator.u[i] -= state_delta[i]
      end
   end
   cb = DiscreteCallback(condition, affect!)
end

trigger_time = [2.0; 4.0]
cb_inflow = discrete_inflow(prob, trigger_time[1], u0)
cb_outflow = discrete_outflow(prob, trigger_time[2], u0)

cbs = CallbackSet(cb_inflow, cb_outflow)

sol = solve(prob, Tsit5(), callback = cbs, tstops = trigger_time)
plot(sol)
savefig("fig_2eqs.png")
