# Test callback function for implementing into an SBR

using DifferentialEquations
using Plots
function f(du, u, p, t)
    du[1] = -u[1]
    du[2] = -u[2] + V*sin(2*pi*t)
    du[3] = u[4]
    du[4] = -u[3]
end
u0 = [9.0, 4.5, 1.0, 1.0]
const V = 1
prob = ODEProblem(f, u0, (0.0, 10.0))


"""
test
"""
function discrete_inflow(prob, trigger_time, state_delta)
   condition(u, t, delta_state) = t == trigger_time
   affect!(integrator) = begin
      for i in 1:length(state_delta)
         integrator.u[i] += state_delta[i]
      end
   end
   cb = DiscreteCallback(condition, affect!)
end

function discrete_outflow(prob, trigger_time, state_delta)
   condition(u, t, delta_state) = t == trigger_time
   affect!(integrator) = begin
      for i in 1:length(state_delta)
         integrator.u[i] -= state_delta[i]
      end
   end
   cb = DiscreteCallback(condition, affect!)
end

trigger_time = [2.0; 4.0]
cb_inflow = discrete_inflow(prob, trigger_time[1], u0)
cb_outflow = discrete_outflow(prob, trigger_time[2], u0)

cbs = CallbackSet(cb_inflow, cb_outflow)

sol = solve(prob, Tsit5(), callback = cbs, tstops = trigger_time)
plot(sol)
savefig("fig_2eqs.png")


