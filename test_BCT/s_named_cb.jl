# Test callback function for implementing into an SBR
using ModelingToolkit
using DifferentialEquations
using Plots
using Symbolics

@parameters V
@variables t SS(t) XS(t) HA(t) HB(t)

D = Differential(t)

eqs = [
    D(SS) ~ -SS,
    D(SS) + SS ~ 0,
    D(XS) ~ -XS + V*sin(2*pi*t),
    D(HB) ~ HA,
    D(HA) ~ -HB]
  
@named _sys = ODESystem(eqs, t)
sys = structural_simplify(_sys)


u0 = [9.0, 4.5, 3.0, 1.0]
tspan = (0, 10)
V = 1

prob = ODEProblem(sys, u0, tspan, [V])

"""
test
"""
function callback_factory(prob, trigger_time, state_delta, n)
   condition(u, t, integrator) = t ∈ trigger_time
   affect!(integrator) = integrator.u[n] += state_delta
   cb = DiscreteCallback(condition, affect!)
end

n = 4
trigger_time = [4.0; 8.0]
cb = callback_factory(prob, trigger_time, 4.0, n)

sol = solve(prob, Tsit5(), callback=cb, tstops=trigger_time)
plot(sol)
savefig("fig_2cb.png")


