ASM python code
===============

Python code used for analyses of ASM models.

To generate the documentation run::

make html

in the ``doc`` folder.

The generated documentation is hosted at https://viraja.gitlab.io/automatic_feature_extraction/doc/.
