""" Read ASM1 cycle data in CSV format and plot files for ICA 2022"""

# Copyright (C) 2022 Juan Pablo Carbajal, Mariane Y. Schneider
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Mariane Y. Schneider <myschneider@meiru.ch>
# Date: 20.06.2022
import sys

from typing import Union
from pathlib import Path

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from sbrfeatures.features_O2 import aeration_ramp


def csv_to_df(fpath: Union[str, Path], **kwargs) -> pd.DataFrame:
    df = pd.read_csv(fpath, **kwargs)
    return df


def asm1_csv_file(variable: str):
    return f"OAT_cycles_pert_var{variable}.csv"


_relevant_variables = [
    "SS",
    "SO",
    "SNO",
    "SNH",
    "SND",
    "SALK",
    "XS",
    "XBH",
    "XBA",
    "XP",
    "XND",
]

if __name__ == "__main__":
    folder = sys.argv[1] if len(sys.argv) > 1 else "."
    folder = Path(folder)
    dfs = []
    dfs_amo = []
    for var in _relevant_variables:
        fpath = folder / asm1_csv_file(var)
        df_ = csv_to_df(fpath, usecols=["time", "perturbed_amount", "SO", "SNH"])
        df_.rename(columns={"perturbed_amount": "perturbation"}, inplace=True)
        df_["var"] = var
        dfs_amo.append(df_[["time", "var", "perturbation", "SNH"]])
        dfs.append(df_[["time", "var", "perturbation", "SO"]])

    df = pd.concat(dfs)
    df_multi = df.pivot_table(index="time", columns=("var", "perturbation"))
    # data without column identifiers
    df = df_multi.copy()
    df.columns = range(len(df.columns))

    dfamo = pd.concat(dfs_amo)
    dfamo_multi = dfamo.pivot_table(index="time", columns=("var", "perturbation"))
    # data without column identifiers
    dfamo = dfamo_multi.copy()
    dfamo.columns = range(len(dfamo.columns))

    # Plot data
    df.plot(legend=False, ylabel="SO concentration [$g m^{-3}$]", xlabel="time")
    plt.gca().autoscale(enable=True, axis="y", tight=True)

    dfamo.plot(legend=False, ylabel="SNH concentration [$g m^{-3}$]")
    plt.gca().autoscale(enable=True, axis="y", tight=True)

    # Process the signals with the feature
    aer_ramp = []
    for col, sig in df_multi.iteritems():
        ts, feat, _ = aeration_ramp(
            sig.index,
            sig.to_numpy(),
            smoother=lambda t, s: s,
            minslope=41,
            t_interval=[0.1, 0.8],
        )
        aer_ramp.append(
            dict(
                time=ts,
                value=feat[0],
                slope=feat[1],
                perturbed=col[1],
                perturbation=col[2],
            )
        )
    aer_ramp = pd.DataFrame.from_records(aer_ramp)

    print(aer_ramp.count())

    # Plot features
    # fig, axs = plt.subplots(2, 1, sharex="all")
    # aer_ramp.plot(kind="scatter", x="time", y="slope", ax=axs[0])
    # aer_ramp.plot(kind="scatter", x="time", y="value", ax=axs[1])
    # fig.tight_layout()

    # Plot time vs final ammonium
    fig, ax = plt.subplots(1, 1, sharex="all", tight_layout=True)
    tmp_ = aer_ramp.time.copy()
    # end ammonium for no feature results + shift
    SNH_end = dfamo.iloc[-1, :]
    # no feature times replace with growing values of time
    w = np.argsort(SNH_end[tmp_.isna()])
    w = (w - w.min()) / (w.max() - w.min())
    tmp_[tmp_.isna()] = tmp_.max() + w * (1.0 - tmp_.max())
    ax.scatter(tmp_, SNH_end)
    ax.axvspan(aer_ramp.time.max() + 0.01, ax.get_xlim()[-1], color="red", alpha=0.2)
    # edit tick labels
    xtks = ax.get_xticks().round(2)
    msk = xtks < aer_ramp.time.max()
    xlbl = list(map(str, xtks[msk])) + ["no feature"]
    xtks_end = 0.5 * (aer_ramp.time.max() + 0.01 + ax.get_xlim()[-1])
    ax.set_xticks(xtks[msk].tolist() + [xtks_end], xlbl)
    # add SNH threshodl line
    ax.axhline(1.0, color="k")
    ax.set_xlabel("time ramp occurs")
    ax.set_ylabel("SNH @ process end")

    # figure with one ramp marked
    _tfeat = aer_ramp.time.min()
    _val = aer_ramp.loc[aer_ramp.time.argmin()]["value"]
    fig, ax = plt.subplots(1, 1, sharex="all", tight_layout=True)
    ax.plot(df)
    ax.plot(_tfeat, _val, "o", markeredgecolor="black", alpha=0.5)
    ax.set_xlabel("time")
    ax.set_ylabel("SO concentration [$g m^{-3}$]")

    # Plot sensitivities
    fig, axs = plt.subplots(4, 3, sharey="all", constrained_layout=True)
    fig.supylabel("time")
    fig.supxlabel("perturbation")
    for var_grp, ax in zip(aer_ramp.groupby("perturbed"), axs.flatten()):
        var, grp = var_grp
        ax.plot(grp.perturbation, grp.time, label=var)
        ax.legend()
        # grp.plot(x="perturbation", y="slope", ylabel="slope", label=var, ax=axs[1],
        #         legend=False)
        # grp.plot(x="perturbation", y="value", ylabel="value", label=var, ax=axs[2],
        #         legend=False)
    axs[-1, -1].set_visible(False)
    plt.show()
