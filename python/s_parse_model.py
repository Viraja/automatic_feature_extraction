"""
ASM1 nullcline and fixed points analysis
========================================

Read ASM1 equations from TOML file and run some analysis.

"""

# Copyright (C) 2024 Juan Pablo Carbajal
# Copyright (C) 2024 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>

import platform
import numpy as np
import matplotlib.pyplot as plt

major, minor, _ = platform.python_version_tuple()
if major == "3" and int(minor) >= 11:
    import tomllib
else:
    import tomli as tomllib

from pathlib import Path
from itertools import chain, combinations

from sympy import *

init_printing()

filepath = Path("../data/asm1.toml")
with filepath.open(mode="rb") as f:
    data = tomllib.load(f)

# %%
# Load the model
# --------------


def _tosub(x: str):
    parts = x.split("_")
    if len(parts) > 2:
        return x
    if len(parts) > 1:
        if len(parts[1]) > 1:
            parts[1] = "{" + parts[1] + "}"
    return "_".join(parts)


for k, v in data["states"].items():
    k_ = _tosub(k)
    exec(f"{k} = Symbol('{k_}', real=True, positive=True)")
    print(f"{k}: {eval(k)}: v")
states = [eval(k) for k in data["states"].keys()]
IC = {eval(k): v for k, v in data["initial_conditions"].items()}


for k, v in data["parameters"].items():
    k_ = _tosub(k)
    exec(f"{k} = Symbol('{k_}', real=True, positive=True)")
    print(f"{k}: {eval(k)}: v")
params = [eval(k) for k in data["parameters"].keys()]
paramVals = {eval(k): v for k, v in data["parameter_values"].items()}
# add auxiliary parameter values
for k in params:
    if k not in paramVals:
        paramVals[k] = eval(data["parameters"][str(k)])
        paramVals[k] = paramVals[k].subs(paramVals)

for k, v in data["functions"].items():
    args = ",".join(np.unique([s for s in v if s.isalpha()]))
    exec(f"def {k}({args}): return {v}")
    print(f"{k}: {v}")

auxiliary = {}
for k, v in data["auxiliary_variables"].items():
    # Symbolic name of the auxiliary variable
    exec(f"{k} = Symbol('{k}')")
    ks = eval(k)
    # expression of the auxiliary
    auxiliary[ks] = eval(v)
    print(f"{k}: {auxiliary[ks]}")
aux = list(auxiliary.keys())

odes = {}
for k, v in data["time_derivatives"].items():
    exec(f"odes[{k}] = {v}")

# %%
# Compute Nullclines
# -------------------
# We solve using the auxiliary variables
# We ignore ODEs which are constant


def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1, len(s) + 1))


def zeros(eq, constants):
    """computes all zeros of eq respect to free symbols. Some solutions are equivalent.
    To fix that I need to check if a new solution is equivalent to previous ones.
    something like

    for s in nc:
        for k in s.keys():
            for nk,ns in sol.items():
            nc[i][k].subs(nk, ns).equals(k) # if True solutionis not new
    """
    zz = []
    vars = [s for s in eq.free_symbols if s not in constants]
    for v in powerset(vars):
        sol = solve(eq, v, dict=True)
        if all(s not in zz for s in sol):
            # TODO: this check is a lot more complicated
            zz += sol
    return zz


print("Finding nullclines")
nullc = {}.fromkeys(states)
for k, f in odes.items():
    if not isinstance(f, int):
        print(f"state: {k}")
        n_ = zeros(f, params)
        print(f"found {len(n_)} nullcline conditions")
        nullc[k] = n_

# %%
# Conditions for nullcline in S_O
nullc[S_O]

# %%
# Dynamics on the S_O nullcline
# -----------------------------
# We substitute on the ODEs the conditions for S_O nullcline.
# We ignore all those equations that are constant or empty
ode_nSO = {
    k: x.subs(nullc[S_O][0]).expand().simplify().factor()
    for k, x in odes.items()
    if not isinstance(x, (int, list))
}
ode_nSO


# %%
# S_O dependency (S_O parents)
# -------------------------------
#
#
def _parent(expr, leafs, intermediate, constants):
    if expr in leafs:
        return expr
    else:
        if expr in intermediate:
            # is intermediate
            return [
                expr,
                find_parents(intermediate[expr], leafs, intermediate, constants),
            ]
        else:
            raise ValueError(f"Unkown expression {expr}")


def find_parents(expr, leafs, intermediate, constants):
    args_ = (leafs, intermediate, constants)
    syms_ = [x for x in expr.free_symbols if x not in constants]
    pa = []
    for s in syms_:
        try:
            pa.append(_parent(s, *args_))
        except ValueError as e:
            print(e)
    return pa


def get_elems(tree, elems, retval=None):
    if retval is None:
        retval = []
    if tree in elems and tree not in retval:
        retval.append(tree)
    elif isinstance(tree, list):
        for x in tree:
            get_elems(x, elems, retval)
    return retval


# link to the state through all layers of auxiliaries
paSO = find_parents(odes[S_O], states, auxiliary, params)
# state dependency e.g. states to measure to test causal inference
paSO_states = get_elems(paSO, states)

print("Dependency of 2nd derivative of SO wrt time on the states")
print(paSO)
print(paSO_states)

# %%
# Second derviative S_O
# ----------------------
# we get the structure from paSO
# we compute solutions at each level
dhet, daut = symbols(r"\dot{\text{het}} \dot{\text{aut}}")
ddSO_0 = odes[S_O].diff(aer_gr_het) * dhet + odes[S_O].diff(aer_gr_aut) * daut

dXBH, dsSS, dsSOOH = symbols(r"\dot{X}_{BH} \dot{swSS} \dot{swSOOH}")

het_expr = auxiliary[aer_gr_het]
dhet_expr = (
    het_expr.diff(X_BH) * dXBH
    + het_expr.diff(switchSS) * dsSS
    + het_expr.diff(switchSOKOH) * dsSOOH
)

dXBA, dsSNH, dsSOOA = symbols(r"\dot{X}_{BA} \dot{swSNH} \dot{swSOOA}")

aut_expr = auxiliary[aer_gr_aut]
daut_expr = (
    aut_expr.diff(X_BA) * dXBA
    + aut_expr.diff(switchSNH) * dsSNH
    + aut_expr.diff(switchSOKOA) * dsSOOA
)

ddSO_1 = ddSO_0.subs({dhet: dhet_expr, daut: daut_expr})
# collect factors that depend on SO
SO_dep_ = [switchSOKOA, dsSOOA, dsSOOH, switchSOKOH]
ddSO_1 = ddSO_1.expand().collect(SO_dep_)
# In this case their factors do not depend on SO
factors_expr = [ddSO_1.coeff(x) for x in SO_dep_]
factors = symbols(f"f0:{len(SO_dep_)}")
ddSO_1factors = ddSO_1.subs(zip(factors_expr, factors))
factors = dict(zip(factors, factors_expr))

dSO = Symbol(r"\dot{S_O}")
rev_auxiliary = {v: k for k, v in auxiliary.items()}
dsSOOA_expr = auxiliary[switchSOKOA].diff(S_O).subs(rev_auxiliary).simplify() * dSO
dsSOOH_expr = auxiliary[switchSOKOH].diff(S_O).subs(rev_auxiliary).simplify() * dSO

ddSO_2factors = ddSO_1factors.subs(
    {
        dsSOOA: dsSOOA_expr,
        dsSOOH: dsSOOH_expr,
    }
)
ddSO_2factors = ddSO_2factors.collect(dSO)

ddSO_3factors = (
    ddSO_2factors.subs(auxiliary)
    .expand()
    .collect(factors.keys())
    .collect(dSO)
    .collect(S_O)
)
# re-write some denominators
f_ = lambda p: p**2 + 2 * p * S_O + S_O**2
ddSO_3factors = ddSO_3factors.subs({f_(x): f_(x).factor() for x in (K_OA, K_OH)})
# re-write some parentheses
f_ = lambda p: 1 / (p + S_O) - S_O / (p + S_O) ** 2
ddSO_3factors = ddSO_3factors.subs({f_(x): f_(x).factor() for x in (K_OA, K_OH)})

print("Condition for inflexion point")
print(latex(Eq(ddSO_3factors, 0)))

# %%
# Inflection point when S_NH == 0
# -------------------------------
# case where S_NH = 0
# with this we are testing if the inflection point feature is confounded
# extend

# %%
# We start at the highest level of S_O''=0 and evaluate the factors.
# We identify which factors f0, f1, f2, f3 actually contain S_NH or an auxiliary variable depending on it.
print(factors)

# %%
# f0 and f1 depend directly or indirectly on S_NH.
#
# f0
# ****
# For f0 we see the following chains of dependency:
#
#     chain1: f0 <-- switchSNH <-- SNH
#
#     chain2: f0 <-- dotswSNH <-- [dswitchSNH/dSNH, dotSNH]<-- SNH
#
# We start with the first chain and replace SNH with 0.
# this substitutes in switchSNH S_NH with 0 and saves the result. switchSNH = 0.
switchSNH_SNH0_expr = auxiliary[switchSNH].subs(S_NH, 0)

# %%
# for chain2:
# dotswSNH = d(swSNH)/dSNH * dotSNH
#
# first factor
dswSNH_expr = auxiliary[switchSNH].diff(S_NH)
dswSNH_SNH0_expr = dswSNH_expr.subs(S_NH, 0)

# %%
# second factor, it has its own chain
# chains: dotSNH <-- aer_gr_aut <-- swSNH <-- SNH
aer_gr_aut_SNH0_expr = auxiliary[aer_gr_aut].subs(switchSNH, switchSNH_SNH0_expr)
dotSNH_SNH0_expr = odes[S_NH].subs(
    aer_gr_aut, aer_gr_aut_SNH0_expr
)  # this is not equal to zero.

# %%
# Finally
dsSNH_SNH0_expr = (dswSNH_SNH0_expr * dotSNH_SNH0_expr).expand()

# next is to substitute all in f0. We need first to be able to access f0.
fs = list(factors.keys())
f0_expr = factors[fs[0]]
f0_SNH0_expr = f0_expr.subs({switchSNH: switchSNH_SNH0_expr, dsSNH: dsSNH_SNH0_expr})
f0_SNH0_expr = f0_SNH0_expr.simplify().collect(i_XB)
# %%
# f1
# ****
# For f1 we see the following chains of dependency:
#
# chain1: f1 <-- switchSNH <-- S_NH
#
f1_expr = factors[fs[1]]
f1_SNH0_expr = f1_expr.subs({switchSNH: switchSNH_SNH0_expr})

# %%
# back to the second derivative of SO wrt time
# *********************************************
# We substitute f0 anf f1 back in SO''
ddSO_3factors_SNH0 = ddSO_3factors.subs({fs[0]: f0_SNH0_expr, fs[1]: f1_SNH0_expr})
ddSO_3factors_SNH0 = (
    ddSO_3factors_SNH0.expand()
    .collect(S_O)
    .collect(X_BA * kYASO * u_A)
    .collect(K_NH * K_OA + K_NH * S_O)
    .collect(i_XB)
    .collect(K_NH)
)
f_ = lambda p: p**2 + 2 * p * S_O + S_O**2
ddSO_3factors_SNH0 = ddSO_3factors_SNH0.subs(f_(K_OH), f_(K_OH).factor())


# %%
# Simplify root problem
# -----------------------
#
ddSO_states_expr = ddSO_3factors.subs(dSO, odes[S_O]).subs(factors).subs(auxiliary)
IC_ = IC.copy()
[IC_.pop(x) for x in [S_O, S_NH]]
ddSO_simple_expr = ddSO_states_expr.subs({dsSS: 0, dXBA: 0, dXBH: 0}).subs(IC_)

# dswSNH
aer_gr_aut_states_expr = auxiliary[aer_gr_aut].subs(auxiliary)
aer_gr_aut_simple_expr = aer_gr_aut_states_expr.subs(IC_)
dotSNH_simple_expr = odes[S_NH].subs(aer_gr_aut, aer_gr_aut_simple_expr).subs(auxiliary)
dotSNH_simple_expr = dotSNH_simple_expr.subs(IC_)

# %%
# Finally
dswSNH_states_expr = dswSNH_expr.subs(auxiliary)
dswSNH_simple_expr = dswSNH_states_expr.subs(IC_)
dsSNH_simple_expr = dswSNH_simple_expr * dotSNH_simple_expr


ddSO_simple_expr = ddSO_simple_expr.subs(dsSNH, dsSNH_simple_expr)

ddSO_simple = ddSO_simple_expr.subs(paramVals).simplify()

ddSO_simple_denom = denom(ddSO_simple)
# Has the denominator zeros
roots_ = solve(ddSO_simple_denom)
print(f"ddSO_simple denominator has {len(roots_)} roots.")
# Numerator
ddSO_simple_numer = numer(ddSO_simple).expand().factor()
# S_O zero is root
print(f"ddSO_simple_numer as S_O == 0: {ddSO_simple_numer.subs(S_O, 0)}")


# create a function that simplifies the root problem
def get_ddSO_simple(IC):
    IC_ = IC.copy()
    [IC_.pop(x) for x in [S_O, S_NH]]
    ddSO_simple_expr = ddSO_states_expr.subs({dsSS: 0, dXBA: 0, dXBH: 0}).subs(IC_)

    # dswSNH
    aer_gr_aut_states_expr = auxiliary[aer_gr_aut].subs(auxiliary)
    aer_gr_aut_simple_expr = aer_gr_aut_states_expr.subs(IC_)
    dotSNH_simple_expr = (
        odes[S_NH].subs(aer_gr_aut, aer_gr_aut_simple_expr).subs(auxiliary)
    )
    dotSNH_simple_expr = dotSNH_simple_expr.subs(IC_)

    dswSNH_states_expr = dswSNH_expr.subs(auxiliary)
    dswSNH_simple_expr = dswSNH_states_expr.subs(IC_)
    dsSNH_simple_expr = dswSNH_simple_expr * dotSNH_simple_expr
    ddSO_simple_expr = ddSO_simple_expr.subs(dsSNH, dsSNH_simple_expr)

    ddSO_simple_expr = ddSO_simple_expr.subs(dsSNH, dsSNH_simple_expr)

    ddSO_simple = ddSO_simple_expr.subs(paramVals).simplify()
    return ddSO_simple


def plot_ddSO_simple_IC(ddSO_simple_IC):
    ddSO_simple = ddSO_simple_IC
    pli = plot_implicit(Eq(ddSO_simple, 0), (S_O, 0, 1), (S_NH, 0, 1), depth=2)
    pts, _ = pli[0].get_points()
    pts = np.array([(x_int.mid, y_int.mid) for x_int, y_int in pts])
    # keep only non-zero S_O
    pts = pts[pts[:, 0] > 1e-3]
    vel_simple_expr = [
        e.subs(auxiliary).subs(paramVals) for e in (odes[S_O], odes[S_NH])
    ]
    vel_SO = lambdify([(S_O, S_NH)], vel_simple_expr[0].subs(IC_))
    vel_SNH = lambdify([(S_O, S_NH)], vel_simple_expr[1].subs(IC_))
    vel = np.column_stack([vel_SO(pts.T), vel_SNH(pts.T)])
    return (vel, pts)


steps_n = 5
# reduce the initial conditions
step_sizes = {key: IC[key] / (steps_n) for key in IC}

ddSO_simple_IC = {}
current_IC = IC.copy()
ddSO_simple_IC[0] = get_ddSO_simple(current_IC)
for steps in range(1, (steps_n + 1)):
    print(f"Step {steps}:")
    for key in current_IC:
        if key != S_O:
            current_IC[key] -= step_sizes[key]
        print(f"{key}: {current_IC[key]}")
    print(f"IC({steps} = {current_IC})")
    ddSO_simple_IC[steps] = get_ddSO_simple(current_IC)

pts = {}
for steps in range(0, (steps_n + 1)):
    _, pts[steps] = plot_ddSO_simple_IC(ddSO_simple_IC[steps])

fig, ax = plt.subplots()
for keys, values in pts.items():
    ax.plot(*values[::100].T, ".", alpha=0.5, label=round(1 - keys / (steps_n), 1))
ax.set_title("Inflection points of S_O for different initial conditions")
ax.set_xlabel("S_O")
ax.set_ylabel("S_NH")
plt.tight_layout(pad=2)
plt.legend()
plt.show()

# %%
# Plots
# *****
# pli = plot_implicit(Eq(ddSO_simple.items(),0), (S_O, 0, 1), (S_NH, 0, 1), depth=2)
# pts, _ = pli[0].get_points()
# pts = np.array([(x_int.mid, y_int.mid) for x_int, y_int in pts])
# keep only non-zero S_O
# pts = pts[pts[:,0] > 1e-3]
# vel_simple_expr = [e.subs(auxiliary).subs(paramVals) for e in (odes[S_O], odes[S_NH])]
# vel_SO = lambdify([(S_O, S_NH)], vel_simple_expr[0].subs(IC_))
# vel_SNH = lambdify([(S_O, S_NH)], vel_simple_expr[1].subs(IC_))
# vel = np.column_stack([vel_SO(pts.T), vel_SNH(pts.T)])

# plt.plot(*pts[::100].T, '.', alpha=0.5)
# plt.quiver(*pts[::500].T, *vel[::500].T, scale=10000)
# plt.xlabel("S_O"); plt.ylabel("S_NH")
# plt.show()
