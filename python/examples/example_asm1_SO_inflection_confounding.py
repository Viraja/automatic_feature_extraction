"""
ASM1: confounding on the inflection points of the S_O
============================================================================

"""

# Copyright (C) 2024 Juan Pablo Carbajal
# Copyright (C) 2024 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
import matplotlib.pyplot as plt
import pandas as pd

from sympy import *

from functools import partial
import numpy as np
import numpy.ma as ma

from scipy.integrate import solve_ivp
from scipy.special import expit as sigmoid

try:
    import inflection_SO.parsing as pc
except ModuleNotFoundError:
    import sys
    import os

    sys.path.insert(0, os.path.abspath(".."))
    import inflection_SO.parsing as pc

import inflection_SO.models as models
import inflection_SO.symtools as st
import inflection_SO.signal_processing as sp

from inflection_SO.printing import *
init_printing()

# %%
# Load the model
# --------------
component, context = pc.parse_model("ASM1", path=models.folder)


# %%
# States
# ---------------------
X = component.pop("state_vector")
def get_state(s: str):
    """Get symbol and index of a state. """
    idx_ = list(X).index(context["states"][s])
    return X[idx_], idx_

O2, idxSO2 = get_state("S_O2")

# %%
# Process rates
# -------------
r = Matrix(component["processrates"].sympy_expr.to_list())
Jr = r.jacobian(X)

# %%
# Matrix
# -------------
M = component.pop("matrix_sympy")

# %%
# State derivatives
# ---------------------
dotX = st.dXdt(M, r)
# add areation
aer = symbols(r'\text{aer}', real=True, positive=True)
dotX[idxSO2] += aer
ddotX = st.ddXdt(M, Jr, dotX)

# %%
# Inflection point curve
# -----------------------
ipc = ddotX[idxSO2]

# %%
# Parameter values
# -----------------
param_values = dict(component["parameters"][["sympy", "value"]].set_index("sympy").value)
param_values = st.sort_subs(param_values)

# %%
# replace parameter symbols with values
ipc_pv = ipc.subs(param_values).subs(param_values)
ipc_pv

# %%
# Find on which states the IPC depends on
ipc_states = sorted(tuple(x for x in X if ipc.has(x)), key=str)
ipc_states

# %%
# same for ODE system
dotX_pv = dotX.subs(param_values).subs(param_values)
dO2dt_states = sorted(tuple(x for x in X if dotX_pv[idxSO2].has(x)), key=str)
dO2dt_states

# %%
states_to_set = sorted(list(set(ipc_states) | set(dO2dt_states)), key=str)

# %%
# Numerical check
# -----------------

# %%
# State values initial values
states_iv = component["states"][["sympy", "initial_value"]].set_index("sympy").loc[list(X), "initial_value"]

# %%
# Integrate numerically
Tend = 0.01
sol, dX_np, J_np = sp.integrate_numer(dotX_pv.subs(aer, 200), X, X_iv=states_iv, t_span=(0, Tend))
ipc_np = lambdify([tuple(X)], ipc_pv.subs(aer, 200), "numpy", cse=True)

# %%
# Spline representation
t = np.arange(0, Tend, Tend/100)
y = sol.sol(t)
dx_val = np.zeros_like(t)
ipc_val = np.zeros_like(t)
for k, x_ in enumerate(y.T):
    dx_val[k] = dX_np(x_).flatten()[idxSO2]
    ipc_val[k] = ipc_np(x_)
dx_s = sp.make_interp_spline(t, dx_val, k=5)
ipc_s = dx_s.derivative()

# %%
fig, axs = plt.subplots(nrows=2)
ax = axs[0]
ax.plot(t, dx_val, label="symbolic")
ax.plot(t, dx_s(t), ':', label="numerical")
ax.set_ylabel(latex(O2, mode="inline")+" 1st derivative")

ax = axs[1]
ax.plot(t, ipc_val, label="symbolic")
ax.plot(t, ipc_s(t), ':', label="numerical")
ax.set_ylabel(latex(O2, mode="inline")+" 2nd derivative")
ax.set_xlabel("time")

# %%
# Analysis
# ----------
# %%
# Set limits for O2
O2 = context["states"]["S_O2"]
O2_limits = (O2, 1e-3, 10)

# %%
# Set value for NHx
NHx = context["states"]["S_NHx"]

# %%
# Define aeration


#def sigmoid(x): return (1 + np.exp(-x))**-1


def oxygen_PC(SO, *, K=200.0, setpoint=9.0, max_inflow=50.0):
  SO_error = (SO - setpoint)
  inflow = -K * SO_error
  # saturate at max_inflow
  inflow = max_inflow * sigmoid(-20*SO_error)
  #x = sigmoid(inflow - max_inflow)
  #inflow = x * max_inflow + (1 - x) * inflow
  #inflow = sigmoid(inflow) * inflow
  return inflow

# %%
# Find zeros of ipc for all states with random values
oxyflow = 1000.0  # oxygen intake flow

# initialize random generator
rng = np.random.default_rng(list(map(ord, "abracadabra")))

# initial values of states to set
states_iv = component["states"][["sympy", "initial_value"]].set_index("sympy").loc[states_to_set, "initial_value"]

nsamples = 1500
samples = pd.DataFrame(columns=states_to_set, dtype=float)
for Y in states_to_set:
    if Y == NHx:
        samples.loc[:, Y] = rng.uniform(low=2.0, high=200.0, size=nsamples)
    else:
        samples.loc[:, Y] = rng.uniform(low=1e-3, high=min(max(states_iv[Y] * 10, 10), 5e3), size=nsamples)

if "hits" not in locals():
    hits = dict()
    for Y in states_to_set:
        if Y == O2:
            continue
        other_states = [s for s in states_to_set if s not in (O2, Y)]
        lamb_args_ = [tuple(other_states), (O2, Y)]  # arguments for lambdified expression
        ipc_np = lambdify(lamb_args_, ipc_pv.subs(aer, 0), "numpy", cse=True)
        dotO2_np = lambdify(lamb_args_, dotX_pv[idxSO2].subs(aer, 0), "numpy", cse=True)

        fig, ax = plt.subplots()
        args_ = (O2_limits, (Y, 0, max(20, 3*states_iv[Y])))
        xx, yy = np.meshgrid(np.linspace(*args_[0][1:], 60), np.linspace(*args_[1][1:], 60))
        XY = np.vstack((xx.flatten(), yy.flatten()))
        vv = []
        qq = []
        for j, x_ in enumerate(samples.loc[:, other_states].to_numpy()):
            aer_ = oxygen_PC(XY[0], max_inflow=oxyflow)
            # FIXME: crude approaximation of the effect of aeration
            f_ = (partial(ipc_np, x_)(XY) * (1 + aer_)).reshape(xx.shape)

            # Evaluate O2 time derivative
            do2dt_ = (partial(dotO2_np, x_)(XY) + aer_).reshape(xx.shape)
            # select only relevant inflection points
            f_ = ma.masked_where(do2dt_ <= 0, f_)

            q = ax.contour(xx, yy, f_, [0], alpha=0.1)
            if any(p.size > 0 for p in q.allsegs[0]):
                vv.append(j)
                qq.append(q)

        hits[Y] = {"states": other_states, "samples": np.asarray(vv), "contours": qq}
        if hits[Y]["samples"].size < 1:
            plt.close(fig)
            hits.pop(Y)
        else:
            ax.set_xlabel(latex(O2, mode='inline'))
            ax.set_ylabel(latex(Y, mode='inline'))

# %%
# The numer of random samples of the states values that generated an ipc
hits_sz = {k: v["samples"].size for k, v in hits.items()}
hits_sz


# %%
# Trajectory of point in ipc
# --------------------------

# %%
# ODE definition
dotX_np = lambdify([tuple(X)], dotX_pv.subs(aer, 0), "numpy", cse=True)
def ode(t, y, dir=1.0):
    dy = dotX_np(y).flatten()
    dy[idxSO2] += oxygen_PC(y[idxSO2], max_inflow=oxyflow)
    _low = np.isclose(y, 0)
    _neg = dy < 0
    dy[_low & _neg] = 0
    return dir * dy

Jx = dotX_pv.subs(aer, 0).jacobian(X)
Jx_np = lambdify([tuple(X)], Jx, "numpy", cse=True)
def jac(t, y, dir=1.0):
    return dir * Jx_np(y)

def event_factory(i, Y=0.0):
    def ev_(t, y, dir=1.0):
        if not ev_.switched:
            ev_.direction *= dir
            ev_.switched = True
        return y[i] - Y
    ev_.terminal = True
    ev_.direction = -1 if Y <= 0 else 1
    ev_.switched = False
    return ev_
events = [event_factory(i) for i in range(len(X))]
events += [event_factory(idxSO2, Y=10.0)]
events = None

Tmax = 30.0 / 1440
args_ = dict(fun=ode, jac=jac, t_span=(0, Tmax),
             events=events,
             #first_step=1e-8,
             #max_step=Tmax / 10,
             t_eval=np.linspace(0, Tmax, 50),
             method="Radau")

# %%
# Select plane
#y_state = sorted(hits, key=lambda k: max([y.allsegs[0][0][:,0].max()-y.allsegs[0][0][:,0].min() for y in hits[k]["contours"]]))[0]
#y_state = NHx
#y_state = context["states"]["X_ANO"]
y_state = context["states"]["X_OHO"]

# initial values
iv_def = component["states"][["sympy", "initial_value"]].set_index("sympy")["initial_value"]
iv_def = np.maximum(iv_def, 1.0)  # avoid 0 initial values

f, axs = plt.subplots(nrows=2, tight_layout=True)
axs[0].set_xlabel(latex(O2, mode='inline'))
axs[0].set_ylabel(latex(y_state, mode='inline'))
axs[1].set_xlabel('t')
axs[1].set_ylabel(latex(O2, mode='inline'))
lgd = []
for q, idx_sample in zip(hits[y_state]["contours"], hits[y_state]["samples"]):
    # get point on ipc
    qbb = np.asarray([np.concatenate((p.min(axis=0), p.max(axis=0))) for p in q.allsegs[0]]).max(axis=0).reshape(2, 2)
    qmid = (2, 1200)#np.max(qbb, axis=1)
    _, _, _, ptx, pty, _ = q.find_nearest_contour(*qmid, pixel=False)

    # integrate ODE

    # Generate initial value
    iv = iv_def.copy()
    iv_ = samples.loc[idx_sample].copy()
    iv.loc[iv_.index] = iv_
    iv[O2] = ptx
    iv[y_state] = pty
    iv_ls = iv[list(X)].to_list()

    sol = solve_ivp(**args_, y0=iv_ls)
    sol_r = solve_ivp(**args_, y0=iv_ls, args=(-1.0,))
    if events is not None:
        sz_ = list(map(lambda x: x.size, sol.t_events))
        if any(sz_):
            isz_ = np.argmax(sz_)
            if isz_ == len(X):
                print(f"Max state {X[idxSO2]}")
            else:
                print(f"Null state {X[isz_]}")

            sz_ = list(map(lambda x: x.size, sol_r.t_events))
            if any(sz_):
                isz_ = np.argmax(sz_)
                if isz_ == len(X):
                    print(f"Max state {X[idxSO2]}")
                else:
                    print(f"Null state (r) {X[isz_]}")

    ax = axs[0]
    ax.plot(q.allsegs[0][0][:, 0], q.allsegs[0][0][:, 1], label=latex(NHx, mode='inline') + f"={iv.loc[NHx]:.3f}\n")
    clc = ax.get_lines()[-1].get_color()
    ax.plot(ptx, pty, 'xk')

    ax = axs[1]
    ax.plot(sol.t, sol.y[idxSO2], color=clc)
    ax.plot(-sol_r.t, sol_r.y[idxSO2], color=clc)
    ax.plot(0, ptx, 'xk')

    #ax.annotate(#latex(y_state, mode='inline') + f"={pty:.3f}\n" +
    #            latex(NHx, mode='inline') + f"={iv.loc[NHx]:.3f}\n",
    #            (0, ptx), xytext=(1, 0), textcoords="offset points")
#axs[0].legend()

plt.show()
