"""
ASM1: inflection points of the S_O time series
===============================================

Compute the conditions for an inflection point in the time series of the dissolved
oxygen state :math:`S_O`.


"""

# Copyright (C) 2024 Juan Pablo Carbajal
# Copyright (C) 2024 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
import os
import matplotlib.pyplot as plt

from sympy import *

try:
    import inflection_SO.parsing as pc
except ModuleNotFoundError:
    import sys

    sys.path.insert(0, os.path.abspath(".."))
    import inflection_SO.parsing as pc

import inflection_SO.models as models
import inflection_SO.symtools as st
from inflection_SO.printing import *

init_printing()

# %%
# Load the model
# --------------
component, context = pc.parse_model("ASM1", path=models.folder)

# %%
# States
# ********
t = Symbol("t", real=True)
X = component.pop("state_vector")
X_f = Matrix([Function(x)(t) for x in X])
X_f
# %%
# Processes rates
# *****************
r_f = component.pop("rates_vector")
r = Matrix(symbols(f"r_0:{len(r_f)}"))
r_f


# %%
Jr_f = r_f.jacobian(X)
Jr = Jr_f.subs(zip(r_f, r))
Jr_f

# %%
# Mixing matrix
# *****************
M = component.pop("matrix_sympy")
M

# %%
# Check that M matrix respects conservations
C = component["compositionmatrix_sympy"]
res = C.T * M
res = res.simplify()
res

# %%
# The matrix should be zero if the model does.
# The conversion factors are not essential parameters, if we re-parametrize using molar masses we get
p_ = component["parameters"][["sympy", "value"]].set_index("sympy")
param_values = st.sort_subs(dict(p_.value))  # some are expressions
new_param = {k: v for k, v in param_values.items() if not isinstance(v, Number)}
print(*new_param.items(), sep="\n")

# %%
res.subs(new_param).simplify()

# %%
# Re-parametrize it
pinM = [m for m in M.atoms() if not isinstance(m, Number)]
p_ = symbols(f"p0:{len(pinM)}", real=True, positive=True)
pinM = {p: v for p, v in zip(p_, pinM)}
M = M.subs({v: p for p, v in pinM.items()})
M

# %%
# Re-write the model as
#
# .. math:
#    \dot{X} = M r(X)
dotX_expr_X = M * r
dotX_expr_X

# %%
# Show using rates and parameter names
#
dotX = dotX_expr_X.subs({p: v for p, v in pinM.items()}).subs(zip(r, r_f))
dotX

# %%
# The second derivative w.r.t. time is then
#
# .. math:
#    \ddot{X} = M J_r(X) \dot{X}
ddotX_expr_X = M * Jr * X_f.diff(t)
ddotX_expr_X

# %%
# We extract the second derivative of S_O
SO = context["states"]["S_O2"]
idxSO = list(X).index(SO)
ddotSO_X = ddotX_expr_X[idxSO]
ddotSO_X

# %%
# Show using rates and parameter names
#
ddotSO = ddotSO_X.subs(pinM).subs(zip(r, r_f))
ddotSO

# %%
# Find on which states the IPC depends on
r_ = component["processrates"][["sympy", "sympy_expr", "dependency_states"]]
rates_expr = {r.sympy(*r.dependency_states): r.sympy_expr for _, r in r_.iterrows()}
dotX_expr = {s: e.subs(rates_expr) for s, e in zip(X_f.diff(t), dotX)}
tmp_ = ddotSO.subs(rates_expr).doit().subs(dotX_expr)
ddotSO_deps = sorted(tuple(x for x in X if tmp_.has(x)), key=str)
ddotSO_deps


# %%
# Fix the expression on 2 states
# -------------------------------
# We set all states except 2 of them to a fix value.

# %%
# Parameter values
# ********************
# We build a replacement from parameter symbols to their values
param_values = {k: v.subs(param_values) for k, v in param_values.items()}


# %%
# State values
# ********************
# We build a replacement from state symbols to their values
sel_X = (context["states"]["S_O2"],  context["states"]["S_NHx"])
s_ = component["states"][["sympy", "initial_value"]].set_index("sympy")
state_values = {k: v for k, v in dict(s_.initial_value).items() if k not in sel_X}

# %%
# put them together
values = param_values | state_values

# %%
# Build a replacement for process rates
rates_expr = {r.sympy(*r.dependency_states): r.sympy_expr for _, r in r_.iterrows()}

# %%
# Build a replacement for time derivatives
dotX_expr_v = {s: e.subs(rates_expr).subs(values).simplify() for s, e in zip(X_f.diff(t), dotX)}

# %%
# Replace in equation
ddotSO_s = ddotSO.subs(rates_expr).doit().subs(values).subs(dotX_expr_v).simplify()
ipc = ddotSO_s.subs(values).simplify()
ipc

# %%
# Check that symbols are only states
assert all(
    x in X for x in ipc.atoms(Function, Symbol) if not isinstance(x, Number)
)

# %%
# Plot
# *****
# Get all points in the solution, excluding very small values of S_O
SO_limits = (1e-6, 9)
limits2 = (0, 20)
pts = st.get_implicit_points(ipc, x_var=(sel_X[0],) + SO_limits, y_var=(sel_X[1],) + limits2, depth=1)

# %%
fig, ax = plt.subplots()
ax.plot(*pts[:, ::23], '.')
ax.set_xlabel(latex(sel_X[0]))
ax.set_ylabel(latex(sel_X[1]))

plt.show()
