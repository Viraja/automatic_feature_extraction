"""
ASM1: inflection points of the S_O time series as function of states values
============================================================================

Compute the conditions for an inflection point in the time series of the dissolved
oxygen state :math:`S_O` for different values of the other states.

"""

# Copyright (C) 2024 Juan Pablo Carbajal
# Copyright (C) 2024 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
import os
import matplotlib.pyplot as plt

from sympy import *
from sympy.plotting.plot import MatplotlibBackend

from scipy.integrate import solve_ivp
import numpy as np

try:
    import inflection_SO.parsing as pc
except ModuleNotFoundError:
    import sys

    sys.path.insert(0, os.path.abspath(".."))
    import inflection_SO.parsing as pc

import inflection_SO.models as models
import inflection_SO.symtools as st
from inflection_SO.printing import *

init_printing()

# %%
# Load the model
# --------------
component, context = pc.parse_model("ASM1", path=models.folder)


# %%
# States
# ---------------------
X = component.pop("state_vector")

# %%
# Process rates
# -------------
r = Matrix(component["processrates"].sympy_expr.to_list())
Jr = r.jacobian(X)

# %%
# Matrix
# -------------
M = component.pop("matrix_sympy")

# %%
# State derivatives
# ---------------------
dotX = st.dXdt(M, r)
ddotX = st.ddXdt(M, Jr, dotX)

# %%
# Inflection point curve
# -----------------------
idxSNHx = list(X).index(context["states"]["S_NHx"])
ipc = ddotX[idxSNHx]

# %%
# Parameter values
# -----------------
param_values = dict(component["parameters"][["sympy", "value"]].set_index("sympy").value)
param_values = st.sort_subs(param_values)

# %%
# replace parameter symbols with values
ipc_pv = ipc.subs(param_values).subs(param_values)
dotX_pv = dotX.subs(param_values).subs(param_values)
Jx = dotX_pv.jacobian(X)
Jx_sp = np.asarray([int(x) if isinstance(x, Number) else 1 for x in Jx]).reshape(Jx.shape)

# %%
ipc_pv

# %%
# Lambdify
# --------
ipc_np = lambdify([tuple(X)], ipc_pv, "numpy", cse=True)
dotX_np = lambdify([tuple(X)], dotX_pv, "numpy", cse=True)
Jx_np = lambdify([tuple(X)], Jx, "numpy", cse=True)

# %%
# Simulate initial condition
# --------------------------
s_ = component["states"][["sympy", "initial_value"]].set_index("sympy")
iv = [s_.initial_value[s] for s in X]
idxSO2 = list(X).index(context["states"]["S_O2"])


def sigmoid(x): return (1 + np.exp(-x))**-1

def oxygen_PC(SO, *, K=200.0, setpoint=2.0, max_inflow=50.0):
  SO_error = (SO - setpoint)
  inflow = -K * SO_error
  # saturate at max_inflow
  x = sigmoid(inflow - max_inflow)
  inflow = x * max_inflow + (1 - x) * inflow
  return sigmoid(inflow) * inflow


def ode(t, y):
    dy = dotX_np(y).flatten()
    dy[idxSO2] += oxygen_PC(y[idxSO2], setpoint=9.0, max_inflow=400.0)
    return dy

def jac(t, y):
    return Jx_np(y)

sol = solve_ivp(ode, t_span=(0, 1), y0=iv, jac_sparsity=Jx_sp, dense_output=True)

# %%
f, ax = plt.subplots()
ax.plot(sol.t, sol.y.T)
ax.set_xlabel('t')
ax.legend(list(map(lambda x: latex(x, mode='inline'), X)), bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)

# %%
f, ax = plt.subplots()
ax.plot(sol.t, sol.y[[idxSO2, idxSNHx]].T)
ax.set_xlabel('t')
ax.legend(list(map(lambda x: latex(x, mode='inline'), [X[i] for i in (idxSO2, idxSNHx)])), bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)

# %%
# Plot IPC
# ---------
idxOther = [n for n in range(len(X)) if n not in [idxSO2, idxSNHx]]

args_ = ((X[idxSO2], 1e-6, 10), (X[idxSNHx], 0, 0.1))
p_ = None
for t0 in sol.t[[0, -1]]:
    ipc_x = ipc_pv.subs([(X[i], sol.sol(t0)[i]) for i in idxOther])
    if p_ is None:
        p_ = plot_implicit(ipc_x, *args_, show=False)
    else:
        p_.append(plot_implicit(ipc_x, *args_, show=False)[0])

backend = MatplotlibBackend(p_)
backend.process_series()
ax = backend.plt
ax.plot(sol.y[idxSO2], sol.y[idxSNHx], '-k', label="sim")
ax.legend()

#p_.show()

plt.show()