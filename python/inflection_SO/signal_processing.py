"""
Tools for processing signals.
"""

# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi@gmail.com>
import numpy as np
from numpy.typing import NDArray
from scipy.interpolate import BSpline, make_interp_spline
from scipy.integrate import OdeSolution, solve_ivp
from sympy import lambdify


def dense_to_spline(sol: OdeSolution, t: NDArray):
    """Spline interpolation of a dense ODE solution.

    TODO: adaptive sampling?
    """
    y = sol(t)
    if y.ndim > 1:
        ax = 1
    else:
        ax = 0
    bspl = make_interp_spline(t, sol(t), axis=ax)
    return bspl


def integrate_numer(dotX, X, X_iv, **kwargs):
    """Integrate numerically a symbolic ODE."""
    dotX_np = lambdify([tuple(X)], dotX, "numpy", cse=True)
    Jx = dotX.jacobian(X)
    Jx_np = lambdify([tuple(X)], Jx, "numpy", cse=True)

    def jac(t, y):
        return Jx_np(y)

    def ode(t, y):
        dy = dotX_np(y).flatten()
        # Hack to avoid negative states
        _low = np.isclose(y, 0)
        _neg = dy < 0
        dy[_low & _neg] = 0

        return dy

    args_def = dict(t_span=(0, 1), dense_output=True, method="Radau")
    args_def.update(kwargs)

    sol = solve_ivp(fun=ode, jac=jac, y0=X_iv, **args_def)
    return sol, dotX_np, Jx_np
