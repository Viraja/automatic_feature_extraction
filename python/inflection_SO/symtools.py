""" Tools for symbolic expressions. """

import pdb

# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi@gmail.com>

import numpy as np
import networkx as nx
from sympy import plot_implicit, lambdify, symbols, Matrix, latex
from sympy import default_sort_key, topological_sort
from itertools import permutations


def get_implicit_points(expr, **kwargs) -> np.ndarray:
    """Extract points of Sympy's implicit plot."""
    defs = dict(show=False)
    defs.update(kwargs)
    pli = plot_implicit(expr, **defs)
    pts, _ = pli[0].get_points()
    pts = np.array([(x_int.mid, y_int.mid) for x_int, y_int in pts]).T
    return pts


def sort_subs(subs: dict | tuple):
    """Topological sort of substitutions.

    See https://stackoverflow.com/questions/36197283/recursive-substitution-in-sympy
    """
    was_dict = False
    if isinstance(subs, dict):
        subs = list(subs.items())
        was_dict = True

    edges = [(i, j) for i, j in permutations(subs, 2) if i[1].has(j[0])]
    subs = topological_sort([subs, edges], default_sort_key)

    if was_dict:
        subs = dict(subs)
    return subs


def dXdt(M: Matrix, r: Matrix, *, values: dict = None) -> Matrix:
    """Get symbolic time derivatives of states."""
    if values is not None:
        M = M.subs(values)
        r = r.subs(values)
    return M * r


def ddXdt(M: Matrix, Jr: Matrix, dotX: Matrix, *, values: dict = None) -> Matrix:
    """Get symbolic second time derivatives of states."""
    if values is not None:
        M = M.subs(values)
        Jr = Jr.subs(values)
        dotX = dotX.subs(values)
    # FIXME: Associativity seems to fail in sympy
    return M * (Jr * dotX)


def dependency_graph(
    eqs, *, vars, eqs_names, symbolic_nodes: bool = False
) -> nx.DiGraph:
    """Generate a dependency graph for the given equations."""
    g = nx.DiGraph()
    g.add_nodes_from(vars + eqs_names)
    for eq, eqn in zip(eqs, eqs_names):
        for x in vars:
            if eq.has(x):
                g.add_edge(x, eqn)
    return g
