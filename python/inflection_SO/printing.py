""" Tool for printing things to sphinx-gallery. """

# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi@gmail.com>

from sympy import latex, multiline_latex, Symbol
from sympy.core.evalf import EvalfMixin
from sympy.matrices.repmatrix import MutableRepMatrix


def print_html(expr):
    return f'{latex(expr, mode="equation*", itex=True)}'


EvalfMixin._repr_html_ = print_html
MutableRepMatrix._repr_html_ = print_html
