# vim: set tabstop=4
# s_ramp_on_asm1.py
#!/usr/bin/env python3

# Copyright (C) 2022 Mariane Yvonne Schneider
# Copyright (C) 2022 Juan Pablo Carbajal
#
# based on Carbajal 2018
# https://gitlab.com/kakila/oldhtml_reports/-/tree/master/public/br-ml

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Date: 20.06.2022

############
## Imports
# built-ins
import platform

# 3rd party
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from sbrfeatures.features_O2 import aeration_ramp
from sbrfeatures.basicfunctions import smooth

# user
from SBRdata.utils import Result, sensorsdict, processrawdata, fscore, contingencytable
from SBRplots.point_features import feature_vs_value, feature_signal_and_output
from SBRdata.dataparser_syntdata import BaseData

############

matplotlib.rcParams["text.latex.unicode"] = True
matplotlib.rcParams["font.family"] = "sans-serif"
matplotlib.rcParams.update({"font.size": 12})
if platform.system() != "Windows":
    matplotlib.rcParams["text.usetex"] = True
    matplotlib.rcParams["font.sans-serif"] = "DejaVu Sans"
    plt.ion()
else:
    matplotlib.rcParams["font.sans-serif"] = "Arial"

# Configure the size of the legend
LEGENDFMT = dict(
    bbox_to_anchor=(0, 1.02, 1.3, 0.3),
    loc="lower left",
    mode="expand",
    ncol=3,
    fontsize="small",
)

# Globals to pass values to plot functions
NH4 = np.array([])
NH4ISLOW = np.array([])
NH4THRESHOLD = 1
CYCLENAMES = np.array([])


def plotramp(
    *,
    xval=None,
    signal=None,
    output=None,
    feature=None,
    axes=None,
    plots=None,
    case=None,
    name=None
):
    """
    Function plots two plots, having the data without valley in the upper
    part and the data with valley in the lowever plot.

    Arguments
    ---------
    None

    Keyword arguments
    -----------------
    xval : string, optional
        Label of xaxis.
    signal : string, optional
        Label of why axis.
    output : axes, optional
        Subplot of effluent.
    feature : Line2D class, optional
    axes : dictionary, optional
    plots : dictionary, optional
    case : classmethod, optional
        Can process a method that classifies the results.
    name : string, optional
        Name of the cycle.

    Returns
    -------
    axes
        an axes instance
    plots
        a plot instance
    """
    if (axes is None) and (plots is None):
        axes, plots = feature_signal_and_output()
        # Rename singal axes
        axes["signal"].set_xlabel("aeration phase completion [%]")
        axes["signal"].set_ylabel("dissolved oxygen")
        # Rename output axes
        axes["output"].set_ylabel("effluent NH$_{4}^{+}$ nitrogen")
        axes["output"].set_ylim(0, NH4.max())
        # add line to plot slope of feature
        (plots["feature_slope"],) = axes["signal"].plot([0, 0], [0, 0], "r-")
        # add line to show threshold in output
        plots["output_threshold"] = axes["output"].axhline(
            y=NH4THRESHOLD, xmin=0, xmax=1, c="black", linewidth=1, linestyle="--"
        )
        handles, _ = axes["signal"].get_legend_handles_labels()
        axes["signal"].legend(handles=handles, **LEGENDFMT)
    else:
        # Update figure
        axes, plots = feature_signal_and_output(
            xval=xval,
            signal=signal,
            output=output,
            feature=(feature[0], feature[1][0]),
            axes=axes,
            plots=plots,
        )
        plots["signal"].set_label(name.replace("_", r"\_"))

        if feature is None:
            feature = (None, None)
        # Update feature, only time and signal value are used
        # if the feature has more, add it after calling this function
        if feature[0] is not None:
            # draw local slope line
            tv = feature[0]
            yv = feature[1][0]
            dyv = feature[1][1]

            dtloc = np.array([-0.1, 0.1])
            plots["feature_slope"].set_data(tv + dtloc, yv + dyv * dtloc)
            plots["feature_slope"].set_visible(True)
            slope_deg = np.real(np.arctan(dyv / yv)) * 180 / np.pi
            plots["feature"].set_label("ramp: slope {:.1f}°".format(slope_deg))
        else:
            plots["feature_slope"].set_visible(False)

        # configure markers according to case
        plots["output"].set_color(case.value.fmt["color"])
        plots["feature"].set(**case.value.fmt)

        # update legend
        handles, _ = axes["signal"].get_legend_handles_labels()
        axes["signal"].legend(handles=handles, **LEGENDFMT)

        print("Showing %s" % name)
        print(case.name)

    return axes, plots


def applyfeature(time, svalue, *, feature, plotfunc=None):
    """
    Apply a feature to a signal.

    Arguments
    ---------
    time : array_like
        Time values.
    signal : array_like
        Signal values of the same length as t.

    Keyword arguments
    -----------------
    feature : function, optional
        Is a function which can identify a feature.

    Returns
    -------
    float, array_like
        Time of feature occurance.
    bool, array_like
        Indicating if the feature has been found (True) or not (False).
    tuple, array_like
        Classification based on control measurements and the occurance of the
        feature.
    """
    # Prepare plot
    doplot = False
    if plotfunc is not None:
        doplot = True
        axes, plots = plotfunc()

    # Number of signals
    N = len(svalue)

    # Detect feature and plot
    hasfeature = np.array([False] * N)
    time_feature = np.array([np.nan] * N)
    case = np.array([None] * N)
    for i, signal in enumerate(svalue):  # s is all sensor values for one cycle
        tv, sv, ss = feature(time, signal)
        if tv is not None:
            hasfeature[i] = True
            time_feature[i] = tv
        # Result type
        case[i] = Result.classify(hasfeature[i], NH4ISLOW[i])

        # If desired, plot the signals
        if doplot:
            axes, plots = plotfunc(
                xval=time,
                signal=(signal, ss),
                output=NH4[i],
                feature=(tv, sv),
                axes=axes,
                plots=plots,
                case=case[i],
                name=CYCLENAMES[i],
            )
            plt.pause(0.1)  # Hack to allow thread synchronization in Windows systems
            key = input("Press q then return to quit, return to continue: ")
            if key == "q":
                quit()
    if doplot:
        plt.close()

    return time_feature, hasfeature, case


if __name__ == "__main__":
    import argparse
    from functools import partial

    ## Parse command line arguments
    parser = argparse.ArgumentParser(description="Run analysis on all DO data.")
    parser.add_argument(
        "-p",
        "--plot",
        help="If present, plots each signal in the dataset.",
        action="store_true",
    )
    args = parser.parse_args()

    plotfunc = None
    if args.plot:
        plotfunc = plotramp

    vars = [
        "XBH",
        "SNH",
        "SS",
        "XND",
        "XS",
        "SND",
        "SO",
        "XBA",
        "SI",
        "SALK",
        "XP",
        "SNO",
        "Xt",
    ]

    DATA = BaseData()
