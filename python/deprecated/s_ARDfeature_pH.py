# vim: set tabstop=4
# s_ARDfeatures_pH.py
#!/usr/bin/env python3

# Copyright (C) 2022 Mariane Yvonne Schneider
# Copyright (C) 2022 Juan Pablo Carbajal
#
# based on Carbajal 2018
# https://gitlab.com/kakila/oldhtml_reports/-/tree/master/public/br-ml

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Date: 26.05.2022

############
## Imports
# built-ins
import platform

# 3rd party

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import ARDRegression
from sklearn.metrics import mean_squared_error, r2_score

from sbrfeatures.basicfunctions import smooth
from sbrfeatures.features_pH import aeration_valley

# user
from SBRplots.point_features import feature_vs_value, feature_signal_and_output
from SBRdata.utils import Result, sensorsdict, processrawdata, fscore, contingencytable

matplotlib.rcParams["font.family"] = "sans-serif"
matplotlib.rcParams.update({"font.size": 12})
if platform.system() != "Windows":
    matplotlib.rcParams["text.usetex"] = True
    matplotlib.rcParams["font.sans-serif"] = "DejaVu Sans"
    plt.ion()
else:
    matplotlib.rcParams["font.sans-serif"] = "Arial"

# Configure the size and position of the legend
LEGENDFMT = dict(
    bbox_to_anchor=(0, 1.02, 1.3, 0.3),
    loc="lower left",
    mode="expand",
    ncol=3,
    fontsize="small",
)

# Globals to pass values to plot functions
NH4 = np.array([])
NH4ISLOW = np.array([])
NH4THRESHOLD = 1
CYCLENAMES = np.array([])


def applyfeature(time, svalue, *, feature, plotfunc=None):
    """
    Applies the feature to the dataset (in this case the pH data of one cycle).
    """
    # Prepare plot
    doplot = False
    if plotfunc is not None:
        doplot = True
        axes, plots = plotfunc()

    # Number of signals
    N = len(svalue)

    # Detect feature and plot
    hasfeature = np.array([False] * N)
    time_feature = np.array([np.nan] * N)
    case = np.array([None] * N)

    for i, signal in enumerate(svalue):  # signal are all pH values of one cycle
        tv, sv, ss = feature(time, signal)
        if tv is not None:
            hasfeature[i] = True
            time_feature[i] = tv
        # Result type
        case[i] = Result.classify(hasfeature[i], NH4ISLOW[i])
        # If desired, plot the signals
        if doplot:
            axes, plots = plotfunc(
                xval=time,
                signal=(signal, ss),
                output=NH4[i],
                feature=(tv, sv),
                axes=axes,
                plots=plots,
                case=case[i],
                name=CYCLENAMES[i],
            )
            plt.pause(0.1)  # Hack for thread synchronization in Windows systems
            key = input("Press q and return to quit, return to continue: ")
            if key == "q":
                quit()
    if doplot:
        plt.close()

    return time_feature, hasfeature, case


if __name__ == "__main__":
    import argparse
    from functools import partial

    ## Parse command line arguments to show plot of every single cycle
    parser = argparse.ArgumentParser(description="Run analysis on all pH data.")
    parser.add_argument(
        "-p",
        "--plot",
        help="If present, plots each singal in the dataset.",
        action="store_true",
    )
    args = parser.parse_args()

    plotfunc = None
    if args.plot:
        plotfunc = plotvalley

    sensorsource = sensorsdict(stype="pH")
    NH4, nonan, sensor = processrawdata(filesdict=sensorsource)
    NH4ISLOW = NH4 <= NH4THRESHOLD

    tminmax = [0.1, 0.99]  # time interval in which to look for valley
    feature_func = {
        "maintained_1": partial(
            aeration_valley,
            t_interval=tminmax,
            smoother=partial(smooth, order=2, freq=2.25),
        ),
        "unmaintained_1": partial(
            aeration_valley,
            t_interval=tminmax,
            smoother=partial(smooth, order=2, freq=1.82),
        ),
        "unmaintained_2": partial(
            aeration_valley,
            t_interval=tminmax,
            smoother=partial(smooth, order=2, freq=2.57),
        ),
        "unmaintained_3": partial(
            aeration_valley,
            t_interval=tminmax,
            smoother=partial(smooth, order=2, freq=1.82),
        ),
        "unmaintained_4": partial(
            aeration_valley,
            t_interval=tminmax,
            smoother=partial(smooth, order=2, freq=1.82),
        ),
    }

    # compare prediction with the measured value.
    hasfeature = dict().fromkeys(sensorsource.keys())
    time_feature = dict().fromkeys(sensorsource.keys())
    cases = dict().fromkeys(sensorsource.keys())
    sensorvalue = dict().fromkeys(sensorsource.keys())
    for sname, s in sensor.items():
        print("** Sensor type: %s" % sname)

        CYCLENAMES = np.array(s.cycle_names())[nonan]
        ## Filter cycles which do not have an ammonium reference measurement
        ## and select aeration phase (phase 4)
        sensorvalue[sname] = s.interp_nan_allphases()[nonan, :]
        svalue = s.interp_nan(phase=4)[nonan, :]
        # time as phase completion 0==start, 1==end
        time = s.completion_phase(4)
        full_time = s.time

        time_feature[sname], hasfeature[sname], cases[sname] = applyfeature(
            time, svalue, plotfunc=plotfunc, feature=feature_func[sname]
        )

        # TFtable = contingencytable(cases[sname])
        # ## Print summary of classification
        # for k, v in TFtable.items():
        #     print('{}:\t{}\t({})'.format(Result[k].value.text, v, k))
        #
        # for beta in [0.5, 1, 2]:
        #     print('F({:.1f})-score: {:.2f}'.format(beta, fscore(TFtable,
        #                                                         beta**2)))

    # ARD
    sensor_type = "maintained_1"
    svalue = sensorvalue[sensor_type]
    time = full_time
    if "reg" not in vars():
        #' ## Regress with ARD and check the weights of each time step
        reg = ARDRegression(n_iter=int(100), compute_score=True)
        reg.fit(svalue, NH4)

        #' Order features with decreasing weight
        o = np.argsort(np.abs(reg.coef_))[::-1]
        idx = o[:16]

    # regress with selected features
    reg_s = ARDRegression(n_iter=int(300))
    reg_s.fit(svalue[:, idx], NH4)
    y_pred = reg_s.predict(svalue[:, idx])

    print("Mean squared error: %.2f " % mean_squared_error(NH4, y_pred))
    # Explained variance score: 1 is perfect prediction
    print("Variance score: %.2f " % r2_score(NH4, y_pred))

    # ARD feature selection and weight
    plt.ion()
    plt.figure(1)
    plt.clf()
    plt.subplot(2, 1, 1)
    plt.plot(time, reg.coef_)
    plt.ylabel("Weight of the model")
    plt.subplot(2, 1, 2)

    s = np.abs(reg.coef_[idx])
    s = 36 * s / s.max() + 16
    plt.scatter(time[idx], np.mean(svalue[:, idx], axis=0), s=s, alpha=0.9)
    # plt.autoscale(enable=False, axis='y', tight=True)
    plt.ylim(7.25, 10)
    plt.autoscale(enable=False, axis="x", tight=True)
    plt.plot(time, svalue.T, "-", linewidth=0.5)
    plt.ylabel("pH")
    plt.show()

    plt.figure(2)
    plt.clf()
    plt.plot(NH4, "o", label="data")
    plt.plot(y_pred, "x", label="pred.")
    plt.ylabel("NH4")
    plt.xlabel("Cycle")
    plt.legend()
    plt.show()

    sensor_type = "maintained_1"

    # only the values above the threshold of 1 mg/L
    reg_s_part = ARDRegression(n_iter=int(300))
    reg_s_part.fit(svalue[~hasfeature[sensor_type]], NH4[~hasfeature[sensor_type]])
    y_pred_part = reg_s_part.predict(svalue[~hasfeature[sensor_type]])

    print(
        "Mean squared error: %.2f "
        % mean_squared_error(NH4[~hasfeature[sensor_type]], y_pred_part)
    )
    # Explained variance score: 1 is perfect prediction
    print(
        "Variance score: %.2f " % r2_score(NH4[~hasfeature[sensor_type]], y_pred_part)
    )

    plt.figure(3)
    # plt.clf()
    plt.plot(NH4[~hasfeature[sensor_type]], "o", label="data")
    plt.plot(y_pred_part, "x", label="pred.")
    plt.ylabel("NH4")
    plt.xlabel("Cycle")
    plt.legend()
    plt.show()

    # plt.figure(3)
    # plt.clf()
    # plt.plot(reg.scores_)
    # plt.xlabel('Iteration')
    # plt.ylabel('Log marginal likelihood')
