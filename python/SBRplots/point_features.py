# vim: set tabstop=4
# point_features.py
#!/usr/bin/env python3
""" Functions to produce plots of point features."""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Maraine Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 27.06.2018

############
## Imports
# 3rd party
import copy
import matplotlib.pyplot as plt
import numpy as np

############


def feature_vs_value(feature, refval, *, color_mask=None):
    """
    Plots the values of a point feature vs a reference value or
    chronological over datetime.

    The feature values are placed in the y-axis and the reference values in
    the x-axis.

    Arguments
    ---------
    feature : feature value for all the reference values. If it is a
        dictionary the keys are set as labels in the plots
    refval : reference values or a datetime vector to show features over
        time.

    Keyword arguments
    -----------------
    color_mask : a dictionary with the same keys as the feature dict
        containing boolean masks. The plots are colored according to the
        mask and its negation.

    Returns
    -------
    axes
        pyplot axes objects
    lines
        pyplot Line2D objects
    """

    f, (ax_nv, ax) = plt.subplots(
        2, 1, gridspec_kw={"height_ratios": [1, 6]}, sharex=True
    )

    # Extract feaure names: if not dict make dict
    try:
        fea_names = list(feature.keys())
    except AttributeError:
        fea_names = ["noname"]
        feature = {fea_names[0]: feature}

    # format for markers
    fmt = dict()
    if len(fea_names) == 1:
        fmt[fea_names[0]] = {
            "marker": "o",
            "markerfacecolor": "None",
            "linestyle": "None",
        }
    elif len(fea_names) == 2:
        fmt[fea_names[0]] = {
            "marker": "o",
            "markerfacecolor": "None",
            "linestyle": "None",
        }
        fmt[fea_names[1]] = {"marker": "o", "linestyle": "None", "markersize": 2}
    else:
        N = len(fea_names)
        for i, n in enumerate(fea_names):
            fmt[n] = {
                "marker": "o",
                "markerfacecolor": "None",
                "linestyle": "None",
                "markersize": 3 * (N - i),
            }

    # lines2D container
    lines = dict()

    # check if the values are masked
    if color_mask is None:
        for n in fea_names:
            n_plot = copy.copy(n)
            n_plot = n.replace("_", " ")
            isNAN = ~np.isfinite(feature[n])
            tmp1 = ax.plot(
                refval[n][~isNAN], feature[n][~isNAN], label=n_plot, **fmt[n]
            )

            # Plot in ax_nv the missing features
            feature_ = np.ones(feature[n][isNAN].shape) * 0.5
            tmp2 = ax_nv.plot(refval[n][isNAN], feature_, label=n_plot, **fmt[n])
            lines[n] = (tmp1, tmp2)
    else:
        color_true = "#51006B"
        color_false = "#FF940B"
        for n in fea_names:
            n_plot = copy.copy(n)
            n_plot = n_plot.replace("_", " ")
            isNAN = ~np.isfinite(feature[n])
            if isinstance(color_mask, dict):
                mask = color_mask[n]
            else:
                mask = color_mask
            falseh = ax.plot(
                refval[mask & ~isNAN],
                feature[n][mask & ~isNAN],
                color=color_false,
                label=n_plot,
                **fmt[n]
            )
            trueh = ax.plot(
                refval[~mask & ~isNAN],
                feature[n][~mask & ~isNAN],
                color=color_true,
                **fmt[n]
            )

            # Plot in ax_nv the missing features
            feature_ = np.ones(feature[n][mask & isNAN].shape) * 0.5
            falseh_ = ax_nv.plot(
                refval[mask & isNAN],
                feature_,
                color=color_false,
                label=n_plot,
                **fmt[n]
            )
            feature_ = np.ones(feature[n][~mask & isNAN].shape) * 0.5
            trueh_ = ax_nv.plot(
                refval[~mask & isNAN], feature_, color=color_true, **fmt[n]
            )

            lines[n] = ((falseh, falseh_), (trueh, trueh_))

    ax_nv.set_ylim(0, 1)
    ax_nv.set_yticks([0.5])
    ax_nv.set_yticklabels(["no \nfeature"])

    ## hide the spines between ax and ax_nv
    ax.spines["top"].set_visible(False)
    ax.xaxis.tick_bottom()
    ax_nv.spines["bottom"].set_visible(False)
    ax_nv.xaxis.tick_top()
    ax_nv.tick_params(labeltop=False)  # don't put tick labels at the top

    return f, (ax, ax_nv), lines


def feature_signal_and_output(
    *, xval=None, feature=None, signal=None, output=None, axes=None, plots=None
):

    # If no axes and plots arguments then we are just creating the axes and plots
    if (axes is None) and (plots is None):
        _, (ax_signal, ax_output) = plt.subplots(
            1,
            2,
            gridspec_kw={"width_ratios": [10, 1]},
        )

        (signal_plt,) = ax_signal.plot(0, 0, color="k", label="")
        (signalsm_plt,) = ax_signal.plot(0, 0, color="b", label="smoothed")

        (feature_plt,) = ax_signal.plot(0, 0, "go", label="feature")

        ax_signal.autoscale(enable=True, axis="y", tight=False)
        ax_signal.autoscale(enable=True, axis="x", tight=True)

        ax_signal.set_xlabel("xval")
        ax_signal.set_ylabel("signal")
        ax_signal.legend(loc="upper center")

        (output_plt,) = ax_output.plot(0.5, 0, "r+", markeredgewidth=3, markersize=12)

        ax_output.set_xlim(0, 1)
        ax_output.set_xticks([])
        ax_output.set_xticklabels([])
        ax_output.yaxis.tick_right()
        ax_output.yaxis.set_label_position("right")
        ax_output.set_ylabel("Output")

        plt.show(block=False)

        axes = {"signal": ax_signal, "output": ax_output}
        plots = {
            "signal": signal_plt,
            "smoothsignal": signalsm_plt,
            "feature": feature_plt,
            "output": output_plt,
        }
    else:
        # Update output plot
        plots["output"].set_ydata(output)

        # Update singal and smoothed signal
        # if not tuple, then assume smoothed singal is not given
        if isinstance(signal, tuple):
            signal, signal_sm = signal
            plots["smoothsignal"].set_data(xval, signal_sm)
            plots["smoothsignal"].set_visible(True)
        else:
            plots["smoothsignal"].set_visible(False)
        plots["signal"].set_data(xval, signal)

        if feature is None:
            feature = (None, None)
        # Update feature, only time and signal value are used
        # if the feature has more, add it after calling this function
        if feature[0] is None:
            plots["feature"].set_label("no feature")
        else:
            plots["feature"].set_label("feature")
        plots["feature"].set_data(feature[0], feature[1])

        axes["signal"].relim()
        axes["signal"].autoscale_view()

        plt.show(block=False)

    return axes, plots
