=================
API documentation
=================

.. contents:: :local:

Model parsing -- :mod:`inflection_SO.parsing`
--------------------------------------------------------------------------------
.. automodule:: inflection_SO.parsing
   :members:
   :undoc-members:

Model files -- :mod:`inflection_SO.models`
--------------------------------------------------------------------------------
.. automodule:: inflection_SO.models
   :members:
   :undoc-members:
