# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import re
from pathlib import Path
import sphinx_gallery.sorting

src_path = Path("..").resolve() / ".."
sys.path.insert(0, str(src_path))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "Symbolic analyses ASM"
release = "0.0.1"
copyright = "uan Pablo Carbajal, Mariane Yvonne Schneider"
author = "Juan Pablo Carbajal <juanpablo.carbajal@ost.ch> and Mariane Yvonne Schneider <myschneider@meiru.ch>"


# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration
rendermath = (
    "sphinx.ext.mathjax" if "MATHIMG" not in os.environ else "sphinx.ext.imgmath"
)

extensions = [
    rendermath,
    "sphinx.ext.intersphinx",
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "sphinx_autodoc_typehints",
    "sphinx_autodoc_defaultargs",
    "sphinx.ext.autosectionlabel",
    "sphinx_gallery.gen_gallery",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Enable numref
# The numref role is used to reference numbered elements of your documentation.
# For example, tables and images.
numfig = True
math_eqref_format = "eq. ({number})"

# Exclude some urls that always will fail the check
linkcheck_ignore = [r".*doi.org.*"]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinxdoc"
html_logo = ""

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# -- Extension configuration -------------------------------------------------

# -- Options for Autodoc extension ----------------------------------------------
# This value selects if automatically documented members are sorted alphabetical
# (value 'alphabetical'), by member type (value 'groupwise') or by source order
# (value 'bysource'). The default is alphabetical.
autodoc_member_order = "bysource"

# -- Options for Napoleon extension ----------------------------------------------
napoleon_google_docstring = False
napoleon_use_rtype = False
# -- Options for autosectionlabel extension --------------------------------------
# Make sure the target is unique
autosectionlabel_prefix_document = True
# -- Options for typehints extension ---------------------------------------------
typehints_document_rtype = True
typehints_use_rtype = True
# -- Options for Intersphinx extension ----------------------------------------------
intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
    "numpy": ("https://numpy.org/doc/stable/", None),
    "scipy": ("https://docs.scipy.org/doc/scipy/", None),
    "matplotlib": ("https://matplotlib.org/stable/", None),
    "pandas": ("https://pandas.pydata.org/pandas-docs/stable/", None),
}
# -- Options for Gallery extension ----------------------------------------------
sphinx_gallery_conf = {
    # path to your example scripts
    "examples_dirs": [
        "../../examples",
    ],
    # files executed
    "filename_pattern": rf"[{re.escape(os.sep)}]example_",
    # excluded files
    "ignore_pattern": rf"{re.escape(os.sep)}s_|__init__\.py",
    # order of gallery
    "within_subsection_order": sphinx_gallery.sorting.FileNameSortKey,
    # do not reset on each example
    "reset_modules": (),
    # do not capture matplotlib output
    # https://sphinx-gallery.github.io/stable/configuration.html#prevent-capture-of-certain-classes
    "ignore_repr_types": r"matplotlib[.](text|axes|legend|lines)",
    # Show memeory usage
    "show_memory": True,
    # Never fail the build on error
    "only_warn_on_example_error": True,
}
# -- Options for Math rendering ----------------------------------------------
imgmath_image_format = "svg"
imgmath_use_preview = True
imgmath_font_size = 14
# -- Options for Default arguments extension ----------------------------------
rst_prolog = (
    """
.. |default| raw:: html

    <div class="default-value-section">"""
    + ' <span class="default-value-label">Default:</span>'
)
# -- Post process ------------------------------------------------------------
## Do not show default documentation of named tuples
import collections


def remove_namedtuple_attrib_docstring(app, what, name, obj, skip, options):
    if type(obj) is collections._tuplegetter:
        return True
    return skip


def setup(app):
    app.connect("autodoc-skip-member", remove_namedtuple_attrib_docstring)
