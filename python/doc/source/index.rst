Welcome to ASM's documentation!
====================================

.. toctree::
   :maxdepth: 3
   :caption: User manual:

   overview

   auto_examples/index.rst

.. toctree::
   :maxdepth: 2
   :caption: Reference manual:

   API
   references


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
