# A script to run SBR "campaings" (runs with multiple cycles)

include("ASM1.jl")
include("SBR.jl")
include("SBRAeration.jl")

using DataFrames
import CSV

# indexes for the sludge variables
sludge_variables = ASM1.biomass_states[:]
water_variables = ASM1.S_states[:]

# Set up the reactor model
# Aeration
aeration_eqs = [ASM1.odes[:SO].inflow ~ SBRAeration.oxygen_PC(ASM1.odes[:SO].x);]

sbr = SBR.SBRTank(ASM1.rate_eqs, aeration_eqs, ASM1.odes, name=:sbr)
SBR.prime!(sbr, ASM1.default_IC, ASM1.default_params);

# Set up the campaign
n_cycles = 5
volume_setpoint = 1.0
# Tank starts partially filled
sbr.volume = 0.75

# Define SBR controller
function controller(cycle, sbr; volume_setpoint)
  # fixed 4 hour cycle in days
  cycle_duration = 4.0/24.0 
  # Volume of water (m³/cycle) to be added at the beginning of each cycle
  inflow_volume = volume_setpoint - sbr.volume
  inflow_volume = inflow_volume < 0 ? 0.0 : inflow_volume
  # Volume of water (m³/cycle) to be removed at the end of each cycle
  outflow_volume = inflow_volume
  outflow_volume = outflow_volume < 0 ? 0.0 : outflow_volume
  # Return named tuple
  return (; cycle_duration, 
            inflow_volume,
            outflow_volume)
end


# Define inflow concentrations
# inflow concentrations
inflows = [:SS => 45.0, # readily biodegradable substrate (non-VFA) (gCOd/m3)
           :SNH => 24.0, # Total ammonium (gN/m3)
           :SALK => 1000.0, # alkalinity (g/m3) as in Furrer 2018
          ]

# prepare results
col_var = [string(name) => Float64[] for name in sbr.state_names]
results = DataFrame([["cycle" => Float64[], "volume" => Float64[]]; col_var])


# Simulate
sol = nothing
for n = 1:n_cycles
  # record initial state
  push!(results, [-n; sbr.volume; SBR.states(sbr)])

  # compute controller values
  global ctrl = controller(n, sbr, volume_setpoint=volume_setpoint)

  SBR.add_water!(sbr, inflows, volume=ctrl.inflow_volume)
  # record initial state after inflow
  push!(results, [-(n + 0.1); sbr.volume; SBR.states(sbr)])

  # TODO: save DO signal also to file: sol[sbr.state_index[:sO]]
  # run cycle
  global sol = SBR.cycle!(sbr, ctrl.cycle_duration);
  # record final state
  push!(results, [n; sbr.volume; SBR.states(sbr)])

  # remove some tank water
  SBR.remove_water!(sbr, volume=ctrl.outflow_volume)
  # record final state after water removal
  push!(results, [n + 0.1; sbr.volume; SBR.states(sbr)])

end

# Store
CSV.write("sbr_campaing.csv", results)
