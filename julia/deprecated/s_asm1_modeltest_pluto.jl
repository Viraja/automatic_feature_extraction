### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 30a865c8-5a2f-4d63-8853-adae43dabc01
begin
   import Pkg
   Pkg.activate(Base.current_project())
   include("ASM1.jl")
   include("SBR.jl")
   include("SBRAeration.jl")
end

# ╔═╡ 99ee5a7f-8034-47e2-8cf8-d5d30152d204
using DataFrames

# ╔═╡ f01f8403-8438-491c-bf2a-0e67792b655a
md"""
### Is the dissolved oxygen feature visible?
- Which behaviour does the variables in version 0.1 of JASM1 show?
- Does this behaviour make sense?
- What are the next features that need to be implemented?
"""

# ╔═╡ c78f384c-9c42-49d4-b835-4567cfefe9dc
begin
	# indexes for the sludge variables
	sludge_variables = ASM1.biomass_states[:]
	water_variables = ASM1.S_states[:]
end

# ╔═╡ af2afccf-93fe-4917-8ae6-0c0befda4f44
begin
	# Set up the reactor model
	# Aeration
	aeration_eqs = [ASM1.odes[:SO].inflow ~ SBRAeration.oxygen_PC(ASM1.odes[:SO].x);]
	
	sbr = SBR.SBRTank(ASM1.rate_eqs, aeration_eqs, ASM1.odes, name=:sbr)
	SBR.prime!(sbr, ASM1.default_IC, ASM1.default_params);
end

# ╔═╡ 3bbbb009-f5fe-4547-91bf-f3715aecd19a
begin
	# Set up the campaign
	n_cycles = 50
	volume_setpoint = 1.0
	# Tank starts partially filled
	sbr.volume = 0.75
end

# ╔═╡ 3a30ae47-d87d-45a1-beed-1da731985510
begin
	# Define SBR controller
	function controller(cycle, sbr; volume_setpoint, cycle_hours)
	  # fixed 4 hour cycle in days
	  cycle_duration = cycle_hours/24.0
	  # Volume of water (m³/cycle) to be added at the beginning of each cycle
	  inflow_volume = volume_setpoint - sbr.volume
	  inflow_volume = inflow_volume < 0 ? 0.0 : inflow_volume
	  # Volume of water (m³/cycle) to be removed at the end of each cycle
	  outflow_volume = inflow_volume
	  outflow_volume = outflow_volume < 0 ? 0.0 : outflow_volume
	  # Return named tuple
	  return (; cycle_duration, 
	            inflow_volume,
	            outflow_volume)
	end
end

# ╔═╡ c9637bcd-82da-478c-a83f-e048bbe60f6e

# Define inflow concentrations
# inflow concentrations
inflows = [:SS => 45.0, # readily biodegradable substrate (non-VFA) (gCOd/m3)
           :SNH => 24.0, # Total ammonium (gN/m3)
           :SALK => 1000.0, # alkalinity (g/m3) as in Furrer 2018
          ]

# ╔═╡ 8d398368-fbf1-4a1f-83ca-2d33ae6510f5
begin
	# prepare results
	col_var = [string(name) => Float64[] for name in sbr.state_names]
	results = DataFrame([["cycle" => Float64[], "volume" => Float64[]]; col_var])
end

# ╔═╡ 30192ff1-24ac-4b30-9cf0-9d27db071271
begin
	# Simulate
	sol = nothing
	for n = 1:n_cycles
	  # record initial state
	  push!(results, [-n; sbr.volume; SBR.states(sbr)])
	
	  # compute controller values
	  global ctrl = controller(n, sbr, volume_setpoint=volume_setpoint, cycle_hours = 24.0)
	
	  SBR.add_water!(sbr, inflows, volume=ctrl.inflow_volume)
	  # record initial state after inflow
	  #push!(results, [-(n + 0.1); sbr.volume; SBR.states(sbr)])
	
	  # TODO: save DO signal also to file: sol[sbr.state_index[:sO]]
	  # run cycle
	  global sol = SBR.cycle!(sbr, ctrl.cycle_duration);
	  # record final state
	  push!(results, [n; sbr.volume; SBR.states(sbr)])
	
	  # remove some tank water
	  SBR.remove_water!(sbr, volume=ctrl.outflow_volume)
	  # record final state after water removal
	  #push!(results, [n + 0.1; sbr.volume; SBR.states(sbr)])
	
	end
end

# ╔═╡ 89cfa157-5e06-42c3-bdee-abd10c3755f3
begin
	x_cycle = results.cycle[results.cycle .>0]
	biomass = results.XBH[results.cycle .>0] .+ results.XBA[results.cycle .>0]
end


# ╔═╡ df99412c-f442-4c3f-b038-045559d57355
begin
	# Plots
	using Plots
	p1 = plot(x_cycle, [results.XBH[results.cycle .>0], results.XBA[results.cycle .>0]], xlabel = "cycle", ylabel = "XBH and XBA", title = "biomass at end of cycles")
	p2 = plot(sol, vars=[SBR.symbolic_variable(sbr, v, :x) for v in [:SO]], title = "soluble one cycle")
	p3 = plot(sol, vars=[SBR.symbolic_variable(sbr, v, :x) for v in [:XBA, :XBH]], title = "biomass one cycle")
	p4 = plot(sol, vars=[SBR.symbolic_variable(sbr, :SO, v) for v in [:x, :inflow]], title = "oxygen one cycle")
	plotd = plot(p1, p2, p3, p4, layout = (2,2))
	
	# savefig(plotd, "fig01_variables_284h.png")
end

# ╔═╡ 26fa8660-e7cc-11ec-031a-37400bf8c117
import CSV

# ╔═╡ Cell order:
# ╠═f01f8403-8438-491c-bf2a-0e67792b655a
# ╠═30a865c8-5a2f-4d63-8853-adae43dabc01
# ╠═c78f384c-9c42-49d4-b835-4567cfefe9dc
# ╠═af2afccf-93fe-4917-8ae6-0c0befda4f44
# ╠═3bbbb009-f5fe-4547-91bf-f3715aecd19a
# ╠═3a30ae47-d87d-45a1-beed-1da731985510
# ╠═c9637bcd-82da-478c-a83f-e048bbe60f6e
# ╠═8d398368-fbf1-4a1f-83ca-2d33ae6510f5
# ╠═30192ff1-24ac-4b30-9cf0-9d27db071271
# ╠═89cfa157-5e06-42c3-bdee-abd10c3755f3
# ╠═df99412c-f442-4c3f-b038-045559d57355
# ╠═99ee5a7f-8034-47e2-8cf8-d5d30152d204
# ╠═26fa8660-e7cc-11ec-031a-37400bf8c117
