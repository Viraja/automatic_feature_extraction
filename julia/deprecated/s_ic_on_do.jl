# A script to run SBR "campaings" (runs with multiple cycles)

# initial conditions as sbr.state_names plus inflow
#  :XBH = 220.8197007868136
#  :SNH = 11.53605890106933 + 24
#  :SS = 12.880544141022401 + 45
#  :XND = 0.5998127561253119
#  :XS = 8.827886285561934
#  :SND = 0.5992106528048078
#  :SO = 0.0029932693544758625
#  :XBA = 288.64492643338787
#  :SI = 0.0
#  :SALK = 582.5895545073465 + 1000
#  :XP = 71.7773009304545
#  :SNO = 27.444136688188628
#  :Xt = 0.0

include("ASM1.jl")
include("SBR.jl")
include("SBRAeration.jl")

using DataFrames
import CSV
using Plots

# indexes for the sludge variables
sludge_variables = ASM1.biomass_states[:]
water_variables = ASM1.S_states[:]

# Set up the reactor model
# Aeration
aeration_eqs = [ASM1.odes[:SO].inflow ~ SBRAeration.oxygen_PC(ASM1.odes[:SO].x);]

sbr = SBR.SBRTank(ASM1.rate_eqs, aeration_eqs, ASM1.odes, name=:sbr)
SBR.prime!(sbr, ASM1.default_IC, ASM1.default_params);

# Set up the campaign
# Tank starts filled
sbr.volume = 1.0

cycle_duration = 20 / 24      # days
dt = 5 / 1440                 # time step in minutes
tsteps = 0:dt:cycle_duration
nt = length(tsteps)

## OAT perturbation of initial conditions (sets default IC from ASM1.jl)
default_ic = ASM1.default_IC
ic_perturbation = 1.5           # in units of concetrations

_cycle = Matrix(undef, 0, length(sbr.state_names) + 1) # to accumulate each cycle simulation
sol = nothing
for var in sbr.state_names
  # Simulate
  # run cycle with modified initial state
  current_value = SBR.state(sbr, var)
  println("Perturbing $(var): $(current_value) + $(ic_perturbation)")
  global sol, _ = SBR.cycle(sbr, cycle_duration, 
                     ic=[var => current_value + ic_perturbation],
                     saveat=tsteps);
  # record cycle
  global _cycle = [_cycle; 
                  [repeat([String(var)], nt) Array(sol)']]

end

# Store in DF
results = DataFrame(_cycle, [["perturbed_variable"]; String.(sbr.state_names)])
results.time = repeat(tsteps, length(sbr.state_names))
select!(results, [[1, ncol(results)]; 2:ncol(results)-1])

# Store
CSV.write("OAT_cycles_pert$(ic_perturbation).csv", results)
p1 = plot()
by_perturbed_var = groupby(results, :perturbed_variable)
for ki in 1:length(by_perturbed_var)
  plot!(by_perturbed_var[ki][:, :SO], label=names(by_perturbed_var)[2+ki])
end

plotd = plot(p1, layout = (1,1), title = "SO (perturbation: $ic_perturbation)", ylims=(0.0,2.0))

savefig(plotd, "fig03_perturbing_$(ic_perturbation).png")
