
using Catalyst
using DifferentialEquations
using Plots
using Symbolics
using Latexify
using ModelingToolkit

@parameters muH muA ηg
@parameters KS KOH KNH KOA KNO KX
@parameters bH bA κₐ κₕ ρ₇ fP
@parameters iXE iXB YH YA
@parameters ηₕ


function ASM1test(; params=default_params)

  @variables t

  switch_func(x, y) = x ./ (x .+ y)

  @species SS(t) XS(t) XBH(t) XBA(t) XP(t) SO(t) SNO(t) SNH(t) SND(t) XND(t) SALK(t)

  rxs = [
    Reaction(muH * switch_func(SS, KS) * switch_func(SO, KOH) * XBH, [SS, SO, SNH, SALK], [XBH], [1/YH, (1-YH)/YH, iXB, iXB/14], [1], only_use_rate=true),
    Reaction(muH * ηg * switch_func(SS, KS) * switch_func(KOH, SO) * switch_func(SNO, KNO) * XBH, [SS, SNO, SNH], [XBH, SALK], [(1/YH), ((1 - YH) / (2.86 * YH)), iXB ], [1, ((1 - YH) / (14 * 2.86 * YH) - iXB / 14)], only_use_rate=true),
    Reaction(muA * switch_func(SNH, KNH) * switch_func(SO, KOA) * XBA, [SO, SNH, SALK], [XBA, SNO], [((4.57 - YA) / YA), (iXB - 1/YA), (iXB / 14 + 1 / (7 * YA))], [1, (1/YA)], only_use_rate=true),
    Reaction(bH * XBH, [XBH], [XS, XP, XND], [1], [(1-fP), fP, (iXB - fP * iXE)], only_use_rate=true),
    Reaction(bA * XBA, [XBA],  [XS, XP, XND], [1], [(1-fP), fP, (iXB - fP * iXE)], only_use_rate=true),
    Reaction(κₐ * SND * XBH, [SND], [SNH, SALK], [14], [14, 1], only_use_rate=true),
    Reaction((κₕ * switch_func(XS, XBH * KX)* ( switch_func(SO, KOH)+ ηₕ * switch_func(KOH, SO) * switch_func(SNO, KNO) ) * XBH), [XS], [SS], only_use_rate=true),
    Reaction(ρ₇ * XND * XS, [XND], [SND], [1], [1], only_use_rate=true)
  ]


  @named rs = ReactionSystem(rxs, t)

  return rxs
end


  default_params = [
                  # stoichiometric parameter (all at 20 C)
                  YH => 0.67,     # heterotrophic yield (gCOD/gCOD)
                  YA => 0.24,     # autotrophic yield (gCOD/gCOD)
                  iXB => 0.086,   # nitrogen fraction in biomass gN/gCOD
                  iXE => 0.01,    # nitrogen fraction in endogenous mass (gN/gCOD)
                  fP => 0.08,     # fraction of biomass leading to particulate material (-)
                  # kinetic parameter heterotrophs
                  muH => 6.0,     # maximum specific growth rate (1/day)
                  KS => 20.0,     # substrate saturation constant (gCOD/m3)
                  KOH => 0.2,     # Oxygen saturation constant (gO2/m3)
                  KNO => 0.5,     # nitrate saturation constant (gNO3-N/m3)
                  bH => 0.62,     # specific decay rate (1/day)
                  ηg => 0.8,      # anoxic growth correction factor (-)
                  # kinetic parameter autotrophs
                  muA => 0.8,     # maximum specific growth rate (1/day)
                  KNH => 1.0,     # ammonium saturation constant (gO2/m3)
                  KOA => 0.4,     # oxygen saturation constant (gO2/m3)
                  bA => 0.1,      # specific decay rate (1/day)
                  # hydrolysis parameter
                  κₕ => 3.0,      # maximum specific hydrolysis rate (1/day)
                  ηₕ => 0.4,      # anoxic hydrolysis correction factor
                  KX => 0.03,    # half-saturation coefficient for hydrolysis of XS
                  ρ₇ => 2.0,      # rate of hydrolysis (1/day)
                  # ammonification
                  κₐ => 0.08      # ammonification rate constant
                 ]





rs = ASM1test()
