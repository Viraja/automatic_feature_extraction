using Documenter
makedocs(
    sitename = "BioChemicalTreatment.jl",
    format = :html,
    pages = Any[
        DocPage(
            "index.md",
            "Introduction",
            "Welcome to the documentation for BioChemicalTreatment.jl."
        ),
        DocPage(
            "usage.md",
            "Usage",
            "This page provides examples of how to use BioChemicalTreatment.jl."
        ),
        DocPage(
            "application.md",
            "Application",
            "This page provides examples of how to use BioChemicalTreatment.jl."
        ),
        DocPage(
            "faq.md",
            "FAQ",
            "This page contains the FAQ for BioChemicalTreatment.jl."
        ),
        DocPage(
            "API.md",
            "API",
            "This page contains the API of BioChemicalTreatment.jl."
        )

    ]
)
