module SBRAeration

sigmoid(x) = one(x) / (one(x) + exp(-x))

function oxygen_PC(SO; K=200.0, setpoint=2.0, max_inflow=50.0)
  SO_error = (SO - setpoint)
  inflow = -K * SO_error
  # saturate at max_inflow
  θ = sigmoid(inflow - max_inflow) 
  inflow = θ * max_inflow + (1 - θ) * inflow
  inflow = sigmoid(inflow) * inflow
end

function oxygen_soft_bangbang(SO; K=100.0, setpoint=2.0, max_inflow=500.0)
  SO_error = (SO - setpoint)
  inflow = -K * SO_error
  inflow = sigmoid(inflow) * max_inflow
end

end
