module ASM1
using Printf
# Monolithic ASM1 model
using ModelingToolkit
using ModelingToolkit: t_nounits as t, D_nounits as D

using DifferentialEquations
using Symbolics
using Plots
using Latexify

# @variables t
# soluble states
S_states = [:SI, :SS, :SO, :SNO, :SNH, :SND, :SALK]
# TODO: refine XB states to biomass states, X as particluate states
biomass_states = [:XI, :XS, :XBH, :XBA, :XP, :XND]
states = [S_states; biomass_states]

# Switching function
switch_func(x, y) = x ./ (x .+ y)

@parameters muH muA ηg
@parameters KS KOH KNH KOA KNO KX
@parameters bH bA κₐ κₕ fP
@parameters iXE iXB YH YA
@parameters ηₕ

# Process rates
@. aer_gr_het(SS, SO, XBH)     = muH      * XBH * switch_func(SS, KS) * switch_func(SO, KOH)
@. an_gr_het(SS, SO, SNO, XBH) = muH * ηg * XBH * switch_func(SS, KS) * switch_func(KOH, SO) * switch_func(SNO, KNO)
@. aer_gr_aut(SNH, SO, XBA) = muA * switch_func(SNH, KNH) * switch_func(SO, KOA) * XBA
@. dec_het(XBH) = bH * XBH
@. dec_aut(XBA) = bA * XBA
@. ammon_solorg_N(SND, XBH) = κₐ * SND * XBH
@. hydrol_org(XBH, XS, SO, SNO) = (
                                κₕ * XBH * switch_func(XS, XBH * KX)
                                * ( switch_func(SO, KOH) 
                                + ηₕ * switch_func(KOH, SO) * switch_func(SNO, KNO) ) 
                               )
@. hydrol_org_N(XBH, SO, SNO, XND, XS) = (
                                κₕ * switch_func(XS, (XBH * KX))
                                * ( switch_func(SO, KOH)
                                + ηₕ * switch_func(KOH, SO) * switch_func(SNO, KNO) ) * XBH
                               ) * XND / XS

# State rates
@. rSI() = 0       # inert substrate
@. rXI() = 0
@. rSS(XBH, XS, SO, SNO, SS) = ( 
                              hydrol_org(XBH, XS, SO, SNO) 
                              - (aer_gr_het(SS, SO, XBH) + an_gr_het(SS, SO, SNO, XBH)) / YH
                            )
@. rXS(XBH, XBA, XS, SO, SNO) = (
                              (1 - fP) * dec_het(XBH) + (1 - fP) * dec_aut(XBA) 
                              - hydrol_org(XBH, XS, SO, SNO)
                             )
@. rXBH(SS, SO, SNO, XBH) = aer_gr_het(SS, SO, XBH) + an_gr_het(SS, SO, SNO, XBH) - dec_het(XBH)
@. rXBA(SNH, SO, XBA) = aer_gr_aut(SNH, SO, XBA) - dec_aut(XBA)
@. rXP(XBH, XBA) = fP * (dec_het(XBH) + dec_aut(XBA))
@. rSO(SS, SO, SNH, XBH, XBA) = - (1 - YH) / YH * aer_gr_het(SS, SO, XBH) - (4.57 - YA) / YA * aer_gr_aut(SNH, SO, XBA)
@. rSNO(SS, SO, SNO, SNH, XBH, XBA) = -(1 - YH) / (2.86 * YH) * an_gr_het(SS, SO, SNO, XBH) + 1 / YA * aer_gr_aut(SNH, SO, XBA)
@. rSNH(SS, SO, SNO, SNH, SND, XBH, XBA) = (
                                    ammon_solorg_N(SND, XBH)
                                    - iXB * (aer_gr_het(SS, SO, XBH) + an_gr_het(SS, SO, SNO, XBH))
                                    - (iXB + 1/YA) * aer_gr_aut(SNH, SO, XBA) 
                                   )
@. rSND(SND, SO, SNO, XBH, XND, XS) = hydrol_org_N(XBH, SO, SNO, XND, XS) - ammon_solorg_N(SND, XBH)
@. rXND(XBH, SO, SNO, XBA, XND, XS) = (
                           (iXB - fP * iXE) * (dec_het(XBH) + dec_aut(XBA))
                           - hydrol_org_N(XBH, SO, SNO, XND, XS)
                          )
@. rSALK(SS, SO, SNO, SNH, SND, XBH, XBA) = (
                                     ((1 - YH) / (14 * 2.86 * YH) - iXB / 14) * an_gr_het(SS, SO, SNO, XBH) 
                                     - iXB/14 * aer_gr_het(SS, SO, XBH) 
                                     - (iXB / 14 + 1 / (7 * YA)) * aer_gr_aut(SNH, SO, XBA) 
                                     + 1/14 * ammon_solorg_N(SND, XBH)
                                    )


rates = Dict{Symbol, Any}()
for var in states
  # equivalent to AMS1.r<var>
  rates[var] = getfield(ASM1, Symbol("r", string(var)))
end


function mass_action_factory(has_inflow=false; name)
  #@variables t  
  @variables rate(t) x(t)
  # D = Differential(t)

    if has_inflow
      @variables inflow(t)
      eqs = [D(x) ~ rate + inflow]
    else
      eqs = [D(x) ~ rate]
    end

    ODESystem(eqs, t; name)
end


function method_argnames(m::Method)
    argnames = ccall(:jl_uncompress_argnames, Vector{Symbol}, (Any,), m.slot_syms)
    isempty(argnames) && return argnames
    return argnames[1:m.nargs]
end

# Create equations
odes = Dict{Symbol, ODESystem}()
for var in states
  odes[var] = mass_action_factory(var == :SO, name = var)
end

# Assign functional rates
println("Setting rates:")
rate_eqs =  Equation[]
for var in states
  # Drop #self# argument
  rate = rates[var]
  arg_symbols = method_argnames(methods(rate)[1])[2:end]
  arg_vars = [odes[v].x for v in arg_symbols]
  push!(rate_eqs , odes[var].rate ~ rate(arg_vars...))
  println(@sprintf("  %-6s ↤ ", var), join(arg_symbols, ", "))
end


# default parameter values
# parameters according to Smets et al. 2003 (original source: Henze et al. 1987)
default_params = [
                  # stoichiometric parameter (all at 20 C)
                  YH => 0.67,     # heterotrophic yield (gCOD/gCOD)
                  YA => 0.24,     # autotrophic yield (gCOD/gCOD)
                  iXB => 0.086,   # nitrogen fraction in biomass gN/gCOD
                  iXE => 0.01,    # nitrogen fraction in endogenous mass (gN/gCOD)
                  fP => 0.08,     # fraction of biomass leading to particulate material (-)
                  # kinetic parameter heterotrophs
                  muH => 6.0,     # maximum specific growth rate (1/day)
                  KS => 20.0,     # substrate saturation constant (gCOD/m3)
                  KOH => 0.2,     # Oxygen saturation constant (gO2/m3)
                  KNO => 0.5,     # nitrate saturation constant (gNO3-N/m3)
                  bH => 0.62,     # specific decay rate (1/day)
                  ηg => 0.8,      # anoxic growth correction factor (-)
                  # kinetic parameter autotrophs
                  muA => 0.8,     # maximum specific growth rate (1/day)
                  KNH => 1.0,     # ammonium saturation constant (gO2/m3)
                  KOA => 0.4,     # oxygen saturation constant (gO2/m3)
                  bA => 0.1,      # specific decay rate (1/day)
                  # hydrolysis parameter
                  κₕ => 3.0,      # maximum specific hydrolysis rate (1/day)
                  ηₕ => 0.4,      # anoxic hydrolysis correction factor
                  KX => 0.03,    # half-saturation coefficient for hydrolysis of XS
                  # ammonification
                  κₐ => 0.08      # ammonification rate constant
                 ]


# Default initial conditions:XI, :XS, :XBH, :XBA, :XP, :XND
default_IC = Pair{Num, Float64}[]
for var in states
  x0 = 0.0
  if var == :XBH
    x0 = 221.0
  elseif var == :SNH
    x0 = 50.0
  elseif var == :SS
    x0 = 13.0 
  elseif var == :XND
    x0 = 0.6
  elseif var == :XS
    x0 = 8.8
  elseif var == :SND
    x0 = 0.6
  elseif var == :SO
    x0 = 0.0
  elseif var == :XBA
    x0 = 289.0
  elseif var == :SI
    x0 = 0.0
  elseif var == :SALK
    x0 = 582.6
  elseif var == :XP
    x0 = 72.0
  elseif var == :SNO
    x0 = 27.4
  elseif var == :XI
    x0 = 0.0
  end
  push!(default_IC, odes[var].x => x0)
end

end

