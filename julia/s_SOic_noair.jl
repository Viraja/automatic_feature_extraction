# A script to run SBR with different values of SO and no aeration

include("ASM1.jl")
include("SBR.jl")
include("SBRAeration.jl")

using DataFrames
import CSV
using Plots
using DifferentialEquations

# indexes for the sludge variables
sludge_variables = ASM1.biomass_states[:]
water_variables = ASM1.S_states[:]

# Set up the reactor model
# Aeration to avoid negative SO values
#aeration_eqs = [ASM1.odes[:SO].inflow ~ -ASM1.odes[:SO].rate*SBRAeration.sigmoid(-10*ASM1.odes[:SO].x);]
#aeration_eqs = [ASM1.odes[:SO].inflow ~ 280;]
aeration_eqs = [ASM1.odes[:SO].inflow ~ SBRAeration.oxygen_PC(ASM1.odes[:SO].x; setpoint=9.0, max_inflow=300.0)]

sbr = SBR.SBRTank(ASM1.rate_eqs, aeration_eqs, ASM1.odes, name=:sbr)
SBR.prime!(sbr, ASM1.default_IC, ASM1.default_params);

# Set up the campaign
# Tank starts filled
sbr.volume = 1.0

cycle_duration = 20 / 24         # days
dt = cycle_duration / 100      # time step in days
tsteps = 0:dt:cycle_duration
nt = length(tsteps)

results = DataFrame()

for so in 1:0.1:9.0
  # Simulate
  # run cycle with modified initial state
  println("SO: $(so)")

  global sol, _ = SBR.cycle(sbr, cycle_duration,
                              ic=[:SO => so],
                              saveat=tsteps,alg=OwrenZen5());
  # record cycle
  local _cycle = DataFrame(
    [[repeat([so], nt) Array(sol)' Array(sol[sbr.ode_sys.SO.inflow])];], 
    [["SO_init"]; String.(sbr.state_names); ["aeration"]])
  local _cycle.time = tsteps

    # Store in DF
  append!(results, _cycle)

end
# Store
CSV.write("SOic_noair.csv", results)
