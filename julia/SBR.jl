module SBR
using Symbolics: Num, Equation
using ModelingToolkit: ODESystem, ODEProblem, structural_simplify, compose
import ModelingToolkit: parameters, unknowns
using DifferentialEquations: solve, remake

mutable struct SBRTank
  state_names :: Vector{Symbol}
  state_index :: Dict{Symbol, Int64}
  
  param_names :: Vector{Symbol}
  param_index :: Dict{Symbol, Int64}
  
  volume :: Float64

  ode_sys :: ODESystem

  ode :: ODEProblem

  SBRTank() = new()
end

num_to_symbol(num) = Symbol(split(string(num), "₊")[1])

##############
# Constructors
##############

function complete_SBRTank!(sbr::SBRTank)
  sbr.state_names = num_to_symbol.(symbolic_unknowns(sbr))
  sbr.state_index = Dict((v,n) for (n,v) in enumerate(sbr.state_names))

  sbr.param_names = num_to_symbol.(symbolic_parameters(sbr))
  sbr.param_index = Dict((v,n) for (n,v) in enumerate(sbr.param_names))

  sbr.ode = build_ode(sbr)

  # Current filling volume
  sbr.volume = 0.0

  return nothing
end

function SBRTank(rates::Vector{Equation}, aeration::Vector{Equation}, 
                 state_odes::Vector{ODESystem}; name=:sbr)
  sbr = SBRTank()

  iv = state_odes[1].iv
  sys = compose(ODESystem([rates; aeration], iv, name=name), 
                state_odes...)
  sbr.ode_sys = structural_simplify(sys)

  complete_SBRTank!(sbr)
  return sbr
end

function SBRTank(rates::Vector{Equation}, aeration::Vector{Equation}, 
                 state_odes::Dict{Symbol, ODESystem}; name=:sbr)
  SBRTank(rates, aeration, [values(state_odes)...]; name=name)
end


##########################################
# accessing states and parameters
##########################################

symbolic_unknowns(sbr::SBRTank) = Num.(unknowns(sbr.ode_sys))

unknowns(sbr::SBRTank) = isdefined(sbr, :ode) ? copy(sbr.ode.u0) : zeros(length(symbolic_unknowns(sbr)))

function unknowns(sbr::SBRTank, state::Vector{Pair{Symbol, T}}) where T<:Real
  s = unknowns(sbr)
  for (var, val) in state
    s[sbr.state_index[var]] = val
  end
  return s
end

state(sbr::SBRTank, name::Symbol) = copy(sbr.ode.u0[sbr.state_index[name]])


function symbolic_variable(sbr::SBRTank, name::Symbol, var::Symbol)::Num
    var_sym = Symbol(join(string.([name, var]),"₊"))
    getproperty(sbr.ode_sys, var_sym)
end

function symbolic_variables(sbr::SBRTank, name::Symbol)::Vector{Num}
  name_s = string(name)
  ret = Num[]
  for var in ["x","rate","inflow"]
    var_sym = Symbol(name_s * "₊" * var)
    if hasproperty(sbr.ode_sys, var_sym)
      s_ = getproperty(sbr.ode_sys, var_sym)
      push!(ret, s_)
    end
  end
  return ret
end

# maps the state name with the concentration of the state variable
function state_map(sbr::SBRTank)::Vector{Pair{Num, Float64}}
  Pair.(symbolic_unknowns(sbr), unknowns(sbr))
end

symbolic_parameters(sbr::SBRTank) = Num.(parameters(sbr.ode_sys))

parameters(sbr::SBRTank) = isdefined(sbr, :ode) ? [sbr.ode.ps[v] for v in symbolic_parameters(sbr)] : zeros(length(symbolic_parameters(sbr)))

# It seems we do not use this interface, and it is unclear what was the intention
#= function parameters(sbr::SBRTank, params::Vector{Pair{Symbol, T}}) where T<:Real
  s = Pair.(num_to_symbol.(symbolic_parameters(sbr)), parameters(sbr))
  for (var, val) in params
    s[var] = val
  end
  return s
end
 =#

# maps the parameter name with parameter value
function params_map(sbr::SBRTank)::Vector{Pair{Num, Float64}}
  Pair.(symbolic_parameters(sbr), parameters(sbr))
end

cycle_duration(sbr::SBRTank) = isdefined(sbr, :ode) ? diff([sbr.ode.tspan...])[1] : 0.0

partial_masses(sbr::SBRTank) = unknowns(sbr) .* sbr.volume

partial_masses(sbr::SBRTank, v::T where T<:Real) = unknowns(sbr) .* convert(Float64, v)

##########################################
# inplace setting states and parameters
##########################################

function set_state!(sbr::SBRTank, state::Vector{Pair{Symbol, T}}) where T<:Real
  # update the state
  sbr.ode = remake(sbr.ode, u0=unknowns(sbr, state))
  return nothing
end

function set_state!(sbr::SBRTank, state::Vector{Pair{Num, T}}) where T<:Real
  # update to the state
  set_state!(sbr, [num_to_symbol(var) => val for (var, val) in state])
  return nothing
end

# the parameter are set
function set_params!(sbr::SBRTank, params::Vector{Pair{Symbol, T}}) where T<:Real
  # update the parameters
  sbr.ode = remake(sbr.ode, p=parameters(sbr, params))
  return nothing
end

function set_params!(sbr::SBRTank, params::Vector{Pair{Num, T}}) where T<:Real
  # update the parameters
  for (var, val) in params
    sbr.ode.ps[var] = val
  end
  #set_params!(sbr, [num_to_symbol(var) => val for (var, val) in params])
  return nothing
end

##########################################
# Numerical ODE generation
##########################################

# ODEProblem is set up
function build_ode(sbr::SBRTank, duration=0.0::T where T<:Real)
  u0 = state_map(sbr)
  p = params_map(sbr)
  prob = ODEProblem(sbr.ode_sys, u0, convert(Float64, duration), p, jac=true)
  return prob
end

function prime!(sbr::SBRTank, duration=0.0::T where T<:Real) 
  sbr.ode = build_ode(sbr, duration)
  return nothing
end

function prime!(sbr::SBRTank, ic::Vector{Pair{Num, T}}, 
                params::Vector{Pair{Num, H}}) where {T<:Real, H<:Real}
  set_state!(sbr, ic)
  set_params!(sbr, params)
  sbr.ode = build_ode(sbr)
  return nothing
end

##########################################
# Integration of ODE
##########################################

# ODEProblem is solved
function cycle(sbr::SBRTank, duration::T where T<:Real;
               kwargs...)
  prob = remake(sbr.ode, tspan=convert(Float64, duration));
  sol = solve(prob; kwargs...)
  return (sol, prob)
end

function cycle(sbr::SBRTank, duration::T;
               ic::Vector{Pair{Symbol, H}},
               kwargs...) where {T<:Real, H<:Real}
  prob = remake(sbr.ode, tspan=convert(Float64, duration), 
                u0=unknowns(sbr, ic));
  sol = solve(prob; kwargs...)
  return (sol, prob)
end

function cycle!(sbr::SBRTank, duration::T where T<:Real)
  sol, prob = cycle(sbr, duration)
  sbr.ode = remake(prob, u0=sol[end])
  return sol
end

##########################################
# Actions on the SBR
##########################################
function modified_state(sbr::SBRTank, state::Vector{Pair{Symbol, T}}) where T<:Real
  s = state_map(sbr)
  for (var, val) in state
    s[sbr.state_index[var]] = var => val
  end
  return s
end

# inflow function
function water_addition(sbr::SBRTank, 
                        concetration::Vector{Pair{Symbol, T}}; 
                        volume::H) where {T<:Real, H<:Real}
  # compute current masses of all components
  mass = partial_masses(sbr)
  # add to the current mass the mass in the inflow
  for (var, val) in concetration
    mass[sbr.state_index[var]] += val * volume 
  end
  # compute state (density) using new volume
  new_volume = sbr.volume + volume
  new_state = mass ./ new_volume
  return (new_state, new_volume)
end

# add water action
function add_water!(sbr::SBRTank, concentration::Vector{Pair{Symbol, T}}; 
                    volume::H) where {T<:Real, H<:Real}
  u, v = water_addition(sbr, concentration, volume=volume)
  sbr.ode = remake(sbr.ode, u0=u)
  sbr.volume = v
  return nothing
end

function water_removal(sbr::SBRTank, water_variables::Vector{Symbol};
                       volume::T where T<:Real)
  mass = partial_masses(sbr)
  state = unknowns(sbr)
  mass_flow = Dict{Symbol, Float64}()
  for v in water_variables
    idx = sbr.state_index[v]
    mf = state[idx] * volume
    mass[idx] -= mf
    mass_flow[v] = mf
  end
  new_volume = sbr.volume - volume
  new_state = mass ./ new_volume
  return (new_state, new_volume, (; mass_flow...))
end

# remove water action
function remove_water!(sbr::SBRTank, water_variables::Vector{Symbol};
                       volume::T where T<:Real)
    u, v, mf = water_removal(sbr, water_variables, volume=volume)
    sbr.ode = remake(sbr.ode, u0=u)
    sbr.volume = v
    return mf
end

function remove_water!(sbr::SBRTank; volume::T where T<:Real)
    u, v, mf = water_removal(sbr, sbr.state_names, volume=volume)
    sbr.ode = remake(sbr.ode, u0=u)
    sbr.volume = v
    return mf
end

function sludge_removal(sbr::SBRTank, sludge_variables::Vector{Symbol};
                        wet_volume::T where T<:Real, 
                        sludge_water_fraction::T where T<:Real)
  mass = partial_masses(sbr)
  state = unknowns(sbr)
  mass_flow = Dict{Symbol, Float64}()
  for (v, idx) in sbr.state_index
    mf = state[idx] * wet_volume
    if v ∉ sludge_variables
      mf = mf * sludge_water_fraction
    end
    mass[idx] -= mf
    mass_flow[v] = mf
  end
  new_volume = sbr.volume - wet_volume
  new_state = mass ./ new_volume
  return (new_state, new_volume, (; mass_flow...))
end

# remove sludge action
function remove_sludge!(sbr::SBRTank, sludge_variables::Vector{Symbol};
                        wet_volume::T where T<:Real, 
                        sludge_water_fraction::T where T<:Real)
  u, v, mf = sludge_removal(sbr, sludge_variables, wet_volume=wet_volume, 
                            sludge_water_fraction=sludge_water_fraction)
  sbr.ode = remake(sbr.ode, u0=u)
  sbr.volume = v
  return mf
end

end

