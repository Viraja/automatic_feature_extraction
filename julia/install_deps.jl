# Rn this only once
using Pkg
Pkg.activate(".")
Pkg.add("Revise")
#Pkg.add("Catalyst")
Pkg.add("DifferentialEquations")
Pkg.add("Symbolics")
Pkg.add("ModelingToolkit")
Pkg.add("Plots")
Pkg.add("Pluto")
Pkg.add("Latexify")

# For SBR scripts
Pkg.add("DataFrames")
Pkg.add("CSV")
