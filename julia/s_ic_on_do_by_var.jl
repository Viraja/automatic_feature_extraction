# A script to run SBR "campaings" (runs with multiple cycles)

include("ASM1.jl")
include("SBR.jl")
include("SBRAeration.jl")

using DataFrames
import CSV
using Plots

# indexes for the sludge variables
sludge_variables = ASM1.biomass_states[:]
water_variables = ASM1.S_states[:]

# Set up the reactor model
# Aeration
aeration_eqs = [ASM1.odes[:SO].inflow ~ SBRAeration.oxygen_PC(ASM1.odes[:SO].x;
                                                              max_inflow=310.0);]

sbr = SBR.SBRTank(ASM1.rate_eqs, aeration_eqs, ASM1.odes, name=:sbr)
SBR.prime!(sbr, ASM1.default_IC, ASM1.default_params);

# Set up the campaign
# Tank starts filled
sbr.volume = 1.0

cycle_duration = 20 / 24      # days
dt = 5 / 1440                 # time step in days
tsteps = 0:dt:cycle_duration
nt = length(tsteps)

## OAT perturbation of initial conditions (sets default IC from ASM1.jl)
default_ic = ASM1.default_IC
ic_perturbation = [0, 1.0, 2.0, 4.0, 8.0, 16.0]          # in units of concetrations
ic_perturbation = [-reverse(ic_perturbation)[1:end-1]; ic_perturbation]

for var in sbr.state_names
  if (var == :SI) || (var ==:XI)
    continue
  end

  global _cycle = Matrix(undef, 0, length(sbr.state_names) + 2) # to accumulate each cycle simulation
  global sol = nothing

  local current_value = SBR.state(sbr, var)

  local done_values = []
  local negative_sol_count = 0
  for per in ic_perturbation
    # Expand pertrubation for less influential variables
    if (var ∉ [:SNH, :XND, :SO, :SND])
      per *= 10.0
    end
    if (var == :SNH)
      per *= 2.0
    end

    # Avoid negative initial values and repetitions
    new_value = current_value + per
    new_value = new_value > 0.0 ? new_value : 0.0
    if new_value in done_values
      continue
    else
      push!(done_values, new_value)
    end

    # Simulate
    # run cycle with modified initial state
    println("Perturbing $(var): $(current_value) + $(per)")

    global sol, _ = SBR.cycle(sbr, cycle_duration,
                                ic=[var => new_value],
                                saveat=tsteps);
    # If any concentration is negative skip this result
    if any(any(Array(sol) .< -1e-2))
      println("  Found negative concentrations. Skipping!")
      negative_sol_count += 1
      continue
    end
    # record cycle
    global _cycle = [_cycle;
                      [repeat([per], nt) Array(sol)' Array(sol[sbr.ode_sys.SO.inflow])];
                    ]
  end

  # Store in DF
  global results = DataFrame(_cycle, [["perturbed_amount"]; String.(sbr.state_names); ["aeration"]])
  results.time = repeat(tsteps, length(done_values) - negative_sol_count)
  select!(results, [[1, ncol(results)]; 2:ncol(results)-1])

  # Store
  CSV.write("OAT_cycles_pert_var$(var).csv", results)
end

#p1 = plot()
#perturbed_by = groupby(results, :perturbed_amount)
#for ki in 1:length(perturbed_by)
#  plot!(perturbed_by[ki][:, :SO], label=ic_perturbation[ki], palette = :greenpink)
#end

#var = "SNH"
#plotd = plot(p1, layout = (1,1), title = "perturbed var: $var", ylims=(0.0,2.0))
#savefig(plotd, "fig04_perturbing_$(var).png")
