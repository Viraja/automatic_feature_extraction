gantt
    dateFormat  YYYY-MM-DD
    title       JASM - May to December 2023
    %% excludes    weekends
    %% can be compiled e.g. at mermaid.live

    section Conferences
    Preparation moderation    :           conf1, 2023-09-22, 0.5d
    Dataset preparation       :           conf2, 2023-09-01, 5d
    Co-editing                :           conf3, 2023-12-12, 5d
    Watermatex conference     :milestone, m1, 2023-09-24, 3d

    section Hybrid Modelling
    WRRmod Abstract submission:milestone, conf4, 2023-06-16, 1d

    section Supervision
    Article Beer full read    : done,   sup1, 2023-05-10, 3d
    next reading Ryuichi      : active, sup2, 2023-05-16, 2d

    section JASM
    Documentation                 :active, jasm1, 2023-05-14, 5d
    Abstract WRRmod               :crit, jasm2, after jasm1, 25d
    What to do befor Packaging    :    jasm3, after jasm2, 5d
    API cleaning                  :    jasm4, after jasm2, 5d
    Packaging                     :    jasm5, after jasm2, 3d
    Benchmarking and Optimisation : jasm6, after jasm5, 14d
    Application scenarios         : jasm7, after jasm6, 21d
    JASM published                :milestone, 2023-09-25, 0d

    section Holidays
    Ghent festival             :           holy1, 2023-07-15, 5d
    Hochzeit Sali              :           holy2, 2023-09-14, 3d
    Canada after conference    :           holy3, 2023-10-01, 5d
